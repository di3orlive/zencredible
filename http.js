var express = require("express"),
        http = require('http'),
        bodyParser = require('body-parser'),
        path = require("path"),
        auth = require('./serverauth'),
        admin = require('./serveradmin'),
        book = require('./serverbook'),
        document = require('./serverdocument');

dbconfig = {user: 'Husker', password: '1234',requestTimeout:300000,
    server: '52.35.219.144', database: 'zen', options: {}};
//use localhost in production tcp/ip is turned off
var app = express();
log_rest_queries = true;
app['logpost'] = function(path, func) {
    var local_func = func;
    var wrapper_func = function(request, response, next) {
        if (log_rest_queries)
            console.log(path);
        local_func(request, response, next);
    };
  
    app.post(path,wrapper_func);   
};

app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
auth.init_cache(app);
admin.init_cache(app, auth);
book.init_cache(app);
document.init_cache(app, auth);

app.get('/zen', function(request, response, next) {
    response.sendFile('app.html', {root: './public'});
});
    
http.createServer(app).listen(80);


var crypto = require('crypto');
var utf8 = require('utf8');
S3_generate_signature = function(data, private_key) {
    return crypto.createHmac('sha1', private_key).
            update(utf8.encode(data)).digest('base64');
};
S3_get_full_object_list = function(s3, delete_objects, base_params, object_list, Marker, callback) {

    if ((base_params.Prefix == "") || (base_params.Prefix == "/") || (base_params.Prefix.length < 12))
    {
//error state
//throw "invalid base path";
        callback(base_params, []);
    }

    var params = {
        Bucket: base_params.Bucket,
        Prefix: base_params.Prefix
    };
    if (Marker != null)
        params['Marker'] = Marker;
    s3.listObjects(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            return;
        }

        for (var a = 0, l = data.Contents.length; a < l; a++)
            object_list.push(data.Contents[a]);
        if (data.IsTruncated) {
            S3_get_full_object_list(s3, delete_objects, base_params, object_list, data.NextMarker, callback);
        }

        //use data contents to delete objects
        if (delete_objects && (data.Contents.length > 0)) {
            var obj_to_delete = [];
            for (var a = 0, l = data.Contents.length; a < l; a++)
                obj_to_delete.push({Key: data.Contents[a].Key});
            var local_params = {
                Bucket: base_params.Bucket,
                Delete: {
                    Objects: obj_to_delete,
                    Quiet: true
                }
            };
            s3.deleteObjects(local_params, function(err, data) {
                if (err)
                    console.log(err, err.stack);
                //  else
                //   console.log(data); // successful response
            });
        }
        callback(base_params, object_list);
    });
};