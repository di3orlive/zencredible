var exports = module.exports = {};
var sql = require('mssql');
var AWS = require('aws-sdk');

var app_cache = null;
var auth_cache = null;

exports['init_cache'] = function (app, auth) {
    app_cache = app;
    auth_cache = auth;
    app.logpost('/document/list', function (request, response, next) {
        exports.list(request, response, next);
    });
    app.logpost('/document/get', function (request, response, next) {
        exports.get(request, response, next);
    });
    app.logpost('/document/create', function (request, response, next) {
        exports.create(request, response, next);
    });
    app.logpost('/document/delete', function (request, response, next) {
        exports.delete(request, response, next);
    });
    app.logpost('/document/update', function (request, response, next) {
        exports.update(request, response, next);
    });
    app.logpost('/document/upload_policy', function (request, response, next) {    
        exports.upload_policy(request, response, next);
    });
    app.logpost('/document/search_offerings', function (request, response, next) {
        exports.search_offerings(request, response, next);
    });
};
function add_date_compare_clause(field_name, request_value, comparator, param_list, where_filter) {
    if (request_value != undefined)
        if (request_value != "")
        {
            var d = Date.parse(request_value);
            if (!isNaN(d))
            {
                if (where_filter != "")
                    where_filter += " and ";
                param_list.push(request_value);
                where_filter += " " + field_name + " " + comparator + " cast(@searchparam_" + param_list.length.toString() + " as DateTime) ";
            }
        }
    return where_filter;
}
function retrieve_query_parameter(request, key_name) {
    var key_value = request.body[key_name];
    if (key_value == undefined)
        key_value = "";
    else
        key_value = key_value.trim();

    return key_value;
}
function append_query_parameter(key_name, key_value, param_list, where_filter, wildcardwrapper) {
    if (wildcardwrapper == undefined)
        wildcardwrapper = true;
    if (key_value != "")
    {
        var prefix = "";
        if (where_filter != "")
            prefix += " and ";
        if (wildcardwrapper)
            key_value = "%" + key_value + "%";
        param_list.push(key_value);
        return prefix + " " + key_name + " like  @searchparam_" + param_list.length.toString() + " ";
    }
    return "";
}

exports['list'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }
        var where_filter = "";
        var doc_list = "";
        var detail_json = "{}";
        var max_records = request.body.max_records;
        var browser_token = request.body.browser_token;
        var param_list = [];
        var book_list = retrieve_query_parameter(request, 'book_list');
        if (book_list != "")
        {
            var book_array = book_list.split(",");
            var book_clause = "( ";
            for (var id_book in book_array)
            {
                if (book_clause != "( ")
                    book_clause += " or ";
                param_list.push(book_array[id_book]);
                book_clause += " id_book like @searchparam_" + param_list.length.toString() + " ";
            }
            book_clause += " ) ";
            if (where_filter != "")
                where_filter += " and ";
            where_filter += book_clause;
        }

        var document_name = retrieve_query_parameter(request, 'document_name');
        where_filter += append_query_parameter('document_name', document_name, param_list, where_filter);
        
        var id_document = retrieve_query_parameter(request, 'id_document');
        where_filter += append_query_parameter('id_document', id_document, param_list, where_filter);      
       
        var status = retrieve_query_parameter(request, 'status');
        where_filter += append_query_parameter('status', status, param_list, where_filter, false);
        var doctype = retrieve_query_parameter(request, 'doctype');
        where_filter += append_query_parameter('id_doctype', doctype, param_list, where_filter, false);
        var keywords = retrieve_query_parameter(request, 'keywords');
        if (keywords != "")
        {
            var keyword_clause = "( ";
            var keyword_array = keywords.split(/,| /);
            for (var keyword in keyword_array)
            {
                if (keyword_clause != "( ")
                    keyword_clause += " or ";
                param_list.push("%," + keyword_array[keyword] + ",%");
                keyword_clause += "( search_keyword1 like @searchparam_" + param_list.length.toString() + " or ";
                keyword_clause += " search_keyword2 like  @searchparam_" + param_list.length.toString() + " or ";
                keyword_clause += " search_keyword3 like  @searchparam_" + param_list.length.toString() + " ) ";
            }
            keyword_clause += ") ";
            if (where_filter != "")
                where_filter += " and ";
            where_filter += keyword_clause;
        }

        var published_name = retrieve_query_parameter(request, 'published_name');
        where_filter += append_query_parameter('publish_name', published_name, param_list, where_filter);
        where_filter = add_date_compare_clause("doc_create", request.body["created_before"], "<", param_list, where_filter);
        where_filter = add_date_compare_clause("doc_create", request.body["created_after"], ">", param_list, where_filter);
        where_filter = add_date_compare_clause("doc_edit", request.body["edited_before"], "<", param_list, where_filter);
        where_filter = add_date_compare_clause("doc_edit", request.body["edited_after"], ">", param_list, where_filter);
        where_filter = add_date_compare_clause("date_start", request.body["starts_before"], "<", param_list, where_filter);
        where_filter = add_date_compare_clause("date_start", request.body["starts_after"], ">", param_list, where_filter);
        where_filter = add_date_compare_clause("date_end", request.body["ends_before"], "<", param_list, where_filter);
        where_filter = add_date_compare_clause("date_end", request.body["ends_after"], ">", param_list, where_filter);
        where_filter = add_date_compare_clause("date_publish", request.body["published_before"], "<", param_list, where_filter);
        where_filter = add_date_compare_clause("date_publish", request.body["published_after"], ">", param_list, where_filter);
        var edited_by = retrieve_query_parameter(request, 'edited_by');
        if (edited_by != "")
        {
            if (where_filter != "")
                where_filter += " and ";
            param_list.push("%" + edited_by + "%");
            where_filter += "( edit_user_email like  @searchparam_" + param_list.length.toString() + " or ";
            where_filter += " edit_user_display_name like  @searchparam_" + param_list.length.toString() + " ) ";
        }

        var created_by = retrieve_query_parameter(request, 'created_by');
        if (created_by != "")
        {
            if (where_filter != "")
                where_filter += " and ";
            param_list.push("%" + edited_by + "%");
            where_filter += "( create_user_email like  @searchparam_" + param_list.length.toString() + " or ";
            where_filter += " create_user_display_name like  @searchparam_" + param_list.length.toString() + " ) ";
        }

        if (where_filter != "")
            where_filter += " and ";
        param_list.push(browser_token);
        where_filter += " user_token = @searchparam_" + param_list.length.toString() + " ";
        var sort_column = request.body["sort_column"];
        var sort_order = request.body["sort_order"];
        var sort_description = "document_name";
        if (sort_column != "")
        {
            sort_column = sort_column.toLowerCase().trim();
            if (sort_column == "document_name")
                sort_description = "document_name";
            else if (sort_column == "doc_edit")
                sort_description = "doc_edit";
            else if (sort_column == "doc_create")
                sort_description = "doc_create";
            else if (sort_column == "status")
                sort_description = "status";
            else if (sort_column == "date_publish")
                sort_description = "date_publish";
            else if (sort_column == "publish_name")
                sort_description = "publish_name";
            else if (sort_column == "tags")
                sort_description = "tags";
            else if (sort_column == "id_doctype")
                sort_description = "id_doctype";
        }
        if (sort_order.toLowerCase() == "desc")
            sort_description += " desc";
        var query_string = "select top(@maxrows) *  from auth.documents_4_usertoken ";
        query_string += " where " + where_filter;
        query_string += " order by " + sort_description;
        var sqlRequest = new sql.Request(connection);
        for (var param_ctr = 0; param_ctr < param_list.length; param_ctr++){
            debugger;
            sqlRequest.input("searchparam_" + (param_ctr + 1).toString(), sql.NVarChar(4000), param_list[param_ctr]);
        }                      
        sqlRequest.input("maxrows", sql.INT, parseInt(max_records));
        sqlRequest.query(query_string,
                function (err, recordsets, returnValue) {
                    debugger;
                    if (err != undefined) {
                        console.log('document/list failed query');
                        console.log(err.message);
                        return;
                    }
                    var json_response = {doc_list: recordsets, details: {}};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
exports['get'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }
        var queries = 'select top 1 * from auth.documents_data_4_usertoken ' +
                "where id_document = @id";

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("id", sql.NVarChar(1000), request.body.doc_id);

        sqlRequest.query(queries,
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('document/get failed query');
                        console.log(err.message);
                        return;
                    }

                    var json_response = {doc_record: {update_type: 6}};
                    if (recordsets.length > 0) {
                        json_response = {doc_record: recordsets[0]};
                    }
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
exports['create'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }
        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_book", sql.NVarChar(1000), request.body.id_book);
        sqlRequest.input("document_name", sql.NVarChar(1000), request.body.document_name);
        sqlRequest.input("id_doctype", sql.NVarChar(1000), request.body.id_doctype);
        sqlRequest.input("private_basepath", sql.NVarChar(4000), request.body.private_basepath);
        sqlRequest.input("public_basepath", sql.NVarChar(4000), request.body.public_basepath);
        sqlRequest.input("enabled", sql.NVarChar(1000), request.body.enabled);
        sqlRequest.input("tags", sql.NVarChar(4000), request.body.tags);
        sqlRequest.input("status", sql.NVarChar(1000), request.body.status);
        sqlRequest.input("data_init", sql.NVarChar(sql.MAX), request.body['data_init']);
        sqlRequest.input("data_update", sql.NVarChar(sql.MAX), request.body['data_update']);

        sqlRequest.execute('auth.document_create',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('document/create failed query');
                        console.log(err.message);
                        return;
                    }
                    var json_response = recordsets[0][0];
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
exports['delete'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }
        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_document", sql.NVarChar(1000), request.body.id_document);

        sqlRequest.execute('auth.document_delete',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('document/delete failed query');
                        console.log(err.message);
                        return;
                    }

                    var policy_tbl = recordsets[0];
                    var id_result = policy_tbl[0]['id_result'];
                    if (id_result > 0) {
                        var bucket_name = policy_tbl[0]['bucket_name'];
                        var base_path = policy_tbl[0]['document_publicpath'];

                        var base_params = {
                            Bucket: bucket_name,
                            Prefix: base_path
                        };
                        var s3 = new AWS.S3({accessKeyId: policy_tbl[0]['public_key'],
                            secretAccessKey: policy_tbl[0]['private_key'],
                            region: 'us-east-1'});
                        var object_list = [];
                        S3_get_full_object_list(s3, true, base_params, object_list, null, function (r_base_params, r_object_list) {
                            var json_response = {
                                id_result: "1"
                            };
                            response.writeHead(200, {"Content-Type": "application/json"});
                            response.end(JSON.stringify(json_response));
                        });
                    }
                });
    });
};
exports['update'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }
        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_document", sql.NVarChar(1000), request.body.id_document);
        sqlRequest.input("name", sql.NVarChar(500), request.body.document_name);
        sqlRequest.input("status", sql.NVarChar(1000), request.body.status);
        sqlRequest.input("publish_name", sql.NVarChar(500), request.body.publish_name);
        sqlRequest.input("date_start", sql.NVarChar(100), request.body.date_start);
        sqlRequest.input("date_end", sql.NVarChar(100), request.body.date_end);
        sqlRequest.input("date_publish", sql.NVarChar(100), request.body['date_publish']);
        sqlRequest.input("tags", sql.NVarChar(4000), request.body.search_tags);
        sqlRequest.input("content", sql.NVarChar(sql.MAX), request.body['content']);
        sqlRequest.input("city", sql.NVarChar(500), request.body['city']);
        sqlRequest.input("offering_image", sql.NVarChar(500), request.body['offering_image']);
        sqlRequest.input("promotional_image", sql.NVarChar(500), request.body['promotional_image']);
        sqlRequest.input("category_1", sql.NVarChar(100), request.body['category_1']);
        sqlRequest.input("category_2", sql.NVarChar(100), request.body['category_2']);
        sqlRequest.input("category_3", sql.NVarChar(100), request.body['category_3']);
        sqlRequest.input("min_stay", sql.NVarChar(100), request.body['min_stay']);
        sqlRequest.input("max_stay", sql.NVarChar(100), request.body['max_stay']);
        sqlRequest.input("amenity_list", sql.NVarChar(4000), request.body['amenity_list']);

        sqlRequest.input("is_exclusive", sql.NVarChar(50), request.body['is_exclusive']);
        sqlRequest.input("country", sql.NVarChar(500), request.body['country']);
        sqlRequest.input("street_address", sql.NVarChar(1000), request.body['street_address']);
        sqlRequest.input("apt", sql.NVarChar(100), request.body['apt']);
        sqlRequest.input("zip_code", sql.NVarChar(20), request.body['zip_code']);
        sqlRequest.input("currency", sql.NVarChar(100), request.body['currency']);
        sqlRequest.input("preparation_time", sql.NVarChar(20), request.body['preparation_time']);
        sqlRequest.input("distant_future", sql.NVarChar(20), request.body['distant_future']);
        sqlRequest.input("description", sql.NVarChar(500), request.body['description']);

        sqlRequest.execute('auth.document_update',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('document/update failed query');
                        console.log(err.message);
                        return;
                    }
                    var json_response = {doc_record: recordsets[0][0]};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));

                    auth_cache.init_cache(app_cache);
                });
    });
};
exports['upload_policy'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_document", sql.NVarChar(1000), parseInt(request.body.id_document));
        sqlRequest.input("file_path", sql.NVarChar(4000), request.body.file_path);

        sqlRequest.execute('auth.document_upload_policy', function (err, recordsets, returnValue) {
            if (err != undefined) {
                console.log('document/upload_policy failed query');
                console.log(err.message);
                return;
            }

            var row = recordsets[0][0];
            var id_result = row["id_result"].toString();
            if (parseInt(id_result) > 0) {
                var private_key = row["private_key"].toString();
                var public_key = row["public_key"].toString();
                var policy_text = row["policy_text"].toString();
                var policy_base64 = Buffer(policy_text).toString('base64');
                var acl = row["acl"].toString();
                var target_file_name = row["out_file_name"].toString();

                var form_post = row["bucket_url"].toString();
                var signature = S3_generate_signature(policy_base64, private_key);

                var json_response = {policy_base64: policy_base64,
                    signature: signature,
                    target_file_name: target_file_name,
                    AWSAccessKeyId: public_key,
                    content_type: "",
                    acl: acl,
                    formpost: form_post};

                response.writeHead(200, {"Content-Type": "application/json"});
                response.end(JSON.stringify(json_response));
            }
            else
            {
                console.log('failed document/upload_policy');
            }
        });
    });
};

//anonymous access (dont filter via token)
exports['search_offerings'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        var query_string = "select id from auth.documents where (id_doctype = 1) and (city like  '%' + @destination + '%') "
                + " and cast(@available_date as DateTime) >= isnull(date_start,'2000-01-01T00:00:00.000') "
                + " and ( (cast(@available_date as DateTime) <= isnull(date_end, '2100-01-01T00:00:00.000')) or (date_end = '1900-01-01T00:00:00.000') ) ";

        if (request.body.stay_len > 0) {
            query_string += " and @stay_len >= min_stay and ( (@stay_len <= max_stay) or (max_stay = 0) ) ";
            sqlRequest.input("stay_len", sql.NVarChar(500), request.body.stay_len);
        }
        sqlRequest.input("destination", sql.NVarChar(500), request.body.destination);
        sqlRequest.input("available_date", sql.NVarChar(500), request.body.available_date);

        if (request.body.category != '') {
            query_string += ' and (category_1 like @category or category_2 like @category or category_3 like @category) ';
            sqlRequest.input("category", sql.NVarChar(500), request.body.category);
        }

        //    if (request.body.start_date

        sqlRequest.query(query_string,
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('document/search_offerings failed query');
                        console.log(err.message);
                        return;
                    }
                    var json_response = {offering_list: recordsets, details: {}};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};