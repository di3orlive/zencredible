var global_offering_details_cache = {};
var local_default_gmaps_path = 'https://www.google.com/maps/embed/v1/place?q=restaurants+in+Lincoln,+NE,+United+States&key=AIzaSyCe4mke3MkMFNLPk7N7mwk7KK_K8C1eK9s';
var global_last_search_results = [];
var global_last_search_index_shown = 0;
var global_last_search_result_count_per_fetch = 4;


function __gauth_success_login(data) {
    $('#login_view').modal('hide');
    document.getElementById('outer_wrapper_load').style.display = 'block';

    gauth_browser_info = data;
    gauth_book_list = data.book_list;
    gauth_book_member_list = data.book_member_list;
    gauth_logged_in_user_name = data.profile_doc.display_name;

    //   document.getElementById('loggedin_name').innerHTML = gauth_logged_in_user_name;
    if (gauth_browser_info.profile_doc.id_user > 0) {
        document.getElementById('home_page_logout_anchor').innerHTML =  gauth_logged_in_user_name;
        document.getElementById('home_page_logout_anchor').style.display = 'inline-block';
        document.getElementById('home_page_login_anchor').style.display = 'none';

        //var usertype = gauth_browser_info.profile_doc.hc_type;
        //var c_pwd_btn = document.getElementById('gauth_change_password_btn');
        //c_pwd_btn.style.display = 'inline-block';
    }

    var c_username_display = document.getElementById('loggedin_name');
    search_render_featured_lists();
    c_username_display.style.display = '';

    FillCategories();
}
function search_render_featured_lists() {
    var recommended = gauth_browser_info.offering_lists['recommended'].split(',');
    var trending = gauth_browser_info.offering_lists['trending'].split(',');
    var featured = gauth_browser_info.offering_lists['featured'].split(',');
    var blogs = gauth_browser_info.offering_lists['blogs'].split(',');

    var hp_rec_1 = __search_local_offerings_find_offering(recommended[0]);
    var hp_rec_2 = __search_local_offerings_find_offering(recommended[1]);

    var n_offer_bkg = $('.recommended_offers_list .recommended_offers_left_offer')[0];
    var n_offer_type = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_left_header_section')[0];
    var n_offer_expiration = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_middle_header_section')[0];

    var n_offer_short_description = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_footer_description')[0];
    var n_offer_retail_price = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_original_price')[0];
    var n_offer_actual_price = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_discount_price')[0];

    var n_offer_location_description = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_footer_country_desc')[0];
    var n_offer_length = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_footer_expires_desc')[0];

    // __search_render_offering_node(hp_rec_1, n_offer_type, n_offer_expiration, n_offer_short_description,
    //         n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);



    var n_offer_bkg = $('.recommended_offers_list .recommended_offers_left_offer')[1];
    var n_offer_type = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_left_header_section')[1];
    var n_offer_expiration = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_middle_header_section')[1];

    var n_offer_short_description = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_footer_description')[1];
    var n_offer_retail_price = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_original_price')[1];
    var n_offer_actual_price = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_discount_price')[1];

    var n_offer_location_description = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_footer_country_desc')[1];
    var n_offer_length = $('.recommended_offers_list .recommended_offers_left_offer .rec_offer_footer_expires_desc')[1];

    // __search_render_offering_node(hp_rec_1, n_offer_type, n_offer_expiration, n_offer_short_description,
    //         n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);

    var n_offer_bkg = $('.recommended_offers_list .recommended_offers_right_offer')[0];
    var n_offer_type = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_left_header_section')[0];
    var n_offer_expiration = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_middle_header_section')[0];

    var n_offer_short_description = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_footer_description')[0];
    var n_offer_retail_price = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_original_price')[0];
    var n_offer_actual_price = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_discount_price')[0];

    var n_offer_location_description = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_footer_country_desc')[0];
    var n_offer_length = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_footer_expires_desc')[0];

    // __search_render_offering_node(hp_rec_2, n_offer_type, n_offer_expiration, n_offer_short_description,
    //         n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);

    var n_offer_bkg = $('.recommended_offers_list .recommended_offers_right_offer')[1];
    var n_offer_type = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_left_header_section')[1];
    var n_offer_expiration = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_middle_header_section')[1];

    var n_offer_short_description = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_footer_description')[1];
    var n_offer_retail_price = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_original_price')[1];
    var n_offer_actual_price = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_discount_price')[1];

    var n_offer_location_description = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_footer_country_desc')[1];
    var n_offer_length = $('.recommended_offers_list .recommended_offers_right_offer .rec_offer_footer_expires_desc')[1];

    // __search_render_offering_node(hp_rec_2, n_offer_type, n_offer_expiration, n_offer_short_description,
    //         n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);


    for (var c = 0; c < 6; c++) {
        var rec_data = __search_local_offerings_find_offering(featured[c]);
        var w_class = '.hp_banner_space_offer_wrapper_pos_' + (c + 1).toString();
        var n_offer_bkg = $(w_class)[0];
        var n_offer_type = $(w_class + ' .banner_offer_left_header_section')[0];
        var n_offer_expiration = $(w_class + ' .banner_offer_expiration_footer')[0];

        var n_offer_short_description = $(w_class + ' .banner_offer_footer_description')[0];
        var n_offer_retail_price = $(w_class + ' .banner_offer_original_price')[0];
        var n_offer_actual_price = $(w_class + ' .banner_offer_discount_price')[0];

        var n_offer_location_description = $(w_class + ' .banner_offer_footer_country_desc')[0];
        var n_offer_length = $(w_class + ' .banner_offer_footer_expires_desc')[0];

        // __search_render_offering_node(rec_data, n_offer_type, n_offer_expiration, n_offer_short_description,
        //         n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);
    }

    var hp_rec_1 = __search_local_offerings_find_offering(trending[0]);
    var hp_rec_2 = __search_local_offerings_find_offering(trending[1]);

    var n_offer_bkg = $('.trending_offers_list .recommended_offers_left_offer')[0];
    var n_offer_type = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_left_header_section')[0];
    var n_offer_expiration = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_middle_header_section')[0];

    var n_offer_short_description = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_footer_description')[0];
    var n_offer_retail_price = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_original_price')[0];
    var n_offer_actual_price = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_discount_price')[0];

    var n_offer_location_description = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_footer_country_desc')[0];
    var n_offer_length = $('.rec_offer_footer_expires_desc')[0];

    __search_render_offering_node(hp_rec_1, n_offer_type, n_offer_expiration, n_offer_short_description,
            n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);

    var n_offer_bkg = $('.trending_offers_list .recommended_offers_left_offer')[1];
    var n_offer_type = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_left_header_section')[1];
    var n_offer_expiration = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_middle_header_section')[1];

    var n_offer_short_description = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_footer_description')[1];
    var n_offer_retail_price = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_original_price')[1];
    var n_offer_actual_price = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_discount_price')[1];

//    var n_offer_location_description = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_footer_country_desc')[1];
    var n_offer_location_description = $('.trending_offers_list .recommended_offers_left_offer .rec_offer_footer_country_desc')[1];
    var n_offer_length = $('.rec_offer_footer_expires_desc')[1];

    __search_render_offering_node(hp_rec_1, n_offer_type, n_offer_expiration, n_offer_short_description,
            n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);


    var n_offer_bkg = $('.trending_offers_list .recommended_offers_right_offer')[0];
    var n_offer_type = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_left_header_section')[0];
    var n_offer_expiration = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_middle_header_section')[0];

    var n_offer_short_description = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_footer_description')[0];
    var n_offer_retail_price = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_original_price')[0];
    var n_offer_actual_price = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_discount_price')[0];

    var n_offer_location_description = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_footer_country_desc')[0];
    var n_offer_length = $('.rec_offer_footer_expires_desc')[0];

    __search_render_offering_node(hp_rec_2, n_offer_type, n_offer_expiration, n_offer_short_description,
            n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);

    var n_offer_bkg = $('.trending_offers_list .recommended_offers_right_offer')[1];
    var n_offer_type = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_left_header_section')[1];
    var n_offer_expiration = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_middle_header_section')[1];

    var n_offer_short_description = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_footer_description')[1];
    var n_offer_retail_price = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_original_price')[1];
    var n_offer_actual_price = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_discount_price')[1];

    var n_offer_location_description = $('.trending_offers_list .recommended_offers_right_offer .rec_offer_footer_country_desc')[1];
    var n_offer_length = $('.rec_offer_footer_expires_desc')[1];

    __search_render_offering_node(hp_rec_2, n_offer_type, n_offer_expiration, n_offer_short_description,
            n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);
}
function wishlist_show_add_offering_dialog(offering_id) {
    var offering_data = __search_local_offerings_find_offering(offering_id);
    if (offering_data == null)
        offering_data = global_offering_details_cache[offering_id];
    document.getElementById('wishlist_add_offer_name').innerHTML = __search_local_offerings_find_offering(offering_id).name;
    var wishlist_data = hpwl_fetch_wishlist();

    var wl_picker = document.getElementById('wishlist_add_offer_wishlist_list');
    var wl_picker_html = '';
    for (var c = 0, l = wishlist_data.wishlists.length; c < l; c++)
        wl_picker_html += '<option value="' + c.toString() + '">' + wishlist_data.wishlists[c].name + '</option>';
    wl_picker.innerHTML = wl_picker_html;
    document.getElementById('wishlist_add_offer_id').value = offering_id;
    $('#wishlist_add_offering_dialog').modal('show');
}
function __search_get_clean_content_value(data, value_tag, is_amount) {
    var is_amount = is_amount || false;
    var value = data[value_tag];
    if (!value) {
        value = '';
    } else {
        if ((value.indexOf('$') === -1) && is_amount) {
            value = '$' + value;
        }
    }
    return value;
}

function __search_local_offerings_find_offering(rec_id) {
    for (var c = 0, l = gauth_browser_info.hp_offerings.length; c < l; c++)
        if (gauth_browser_info.hp_offerings[c]['id'] == rec_id)
            return gauth_browser_info.hp_offerings[c];

    return null;
}
function search_offerings_booking_details(offer_data, offer_content) {
    hp_show_offer_detail_view(offer_data, offer_content);
}
function create_booking_from_offering() {
    client_add_booking($('#offering_detail_doc_id').val());
}
function client_add_booking(id_offering) {
    if (gauth_logged_in_user_name == '') {
        alert('not logged in, must be logged in to create bookings');
        return;
    }

    var id_book = gauth_book_list[0].id_book;
    var id_doctype = "2";
    var tags = '';
    var d_row = global_offering_details_cache[id_offering];

    var doc_name = d_row.name + ' BOOKING';
    var doc = {document:
                {
                    nodes: [],
                    properties: {}
                }
    };
    var doc_props = doc.document.properties;
    var data_init = JSON.stringify(doc);
    var doc_add_info = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: id_book,
        document_name: doc_name,
        public_basepath: '',
        private_basepath: '',
        id_doctype: id_doctype,
        enabled: 1,
        tags: tags,
        status: 0,
        data_init: data_init
    };

    gauth_show_busy_icon(1);
    $.post("/document/create", doc_add_info, function (data, status) {
        if (data['id_result'] < 0) {
            gauth_show_error_message(data['description']);
        }
        else {
            var id_document = data['id_result'];
            alert('created' + id_document);
        }

        gauth_show_busy_icon(-1);
    });


}

function hpso_toggle_more_search_options() {
    var more_search_options_div = document.getElementById('home_page_search_options_fields_bar_2');
    var more_search_options_btn = document.getElementById('hpso_more_options_dropdown_button');

    if (more_search_options_div.style.display == 'none') {
        $('#home_page_search_options_fields_bar_2').slideDown(300, function () {
            more_search_options_div.style.display = '';
            more_search_options_btn.innerHTML = "Less Search Options";
        });
    }
    else {
        $('#home_page_search_options_fields_bar_2').slideUp(300, function () {
            more_search_options_div.style.display = 'none';
            more_search_options_btn.innerHTML = "More Search Options";
        });
    }
}


$(document).ready(function () {
     $("#ex2").slider({});
    //   document.getElementById('outer_wrapper_load').style.display = 'block';
    document.getElementById('home_page_zen_logo_wrapper').addEventListener('click', function () {
        hp_show_default_view();
    });
    $('#spso_checkin_field').val(moment(new Date()).format('MM/DD/YYYY'));
    $('.input-daterange').datepicker();

    //only header search field needs keystroke change
    // document.getElementById('home_page_zen_header_search_field').addEventListener('keyup', function () {
    //     __hp_local_update_search_fields('home_page_zen_header_search_field', 'spso_destination_field', 'hpso_destination_field');
    // });
    // document.getElementById('home_page_zen_header_search_field').addEventListener('change', function () {
    //     __hp_local_update_search_fields('home_page_zen_header_search_field', 'spso_destination_field', 'hpso_destination_field');
    // });

    document.getElementById('hpso_destination_field').addEventListener('change', function () {
        __hp_local_update_search_fields('hpso_destination_field', 'spso_destination_field', 'home_page_zen_header_search_field');
    });
    // document.getElementById('hpso_checkin_field').addEventListener('change', function () {
    //     __hp_local_update_search_fields('hpso_checkin_field', 'spso_checkin_field');
    // });
    // document.getElementById('hpso_days_field').addEventListener('change', function () {
    //     __hp_local_update_search_fields('hpso_days_field', 'spso_days_field');
    // });
    // document.getElementById('hpso_orange_search_button').addEventListener('change', function () {
    //     __hp_local_update_search_fields('hpso_orange_search_button', 'spso_orange_search_button');
    // });
    document.getElementById('hpso_guests_field').addEventListener('change', function () {
        __hp_local_update_search_fields('hpso_guests_field', 'spso_guests_field');
    });
    document.getElementById('hpso_rooms_field').addEventListener('change', function () {
        __hp_local_update_search_fields('hpso_rooms_field', 'spso_rooms_field');
    });
    document.getElementById('hpso_price_field').addEventListener('change', function () {
        __hp_local_update_search_fields('hpso_price_field', 'spso_price_field');
    });

    window.addEventListener('scroll', function () {
        var header_elem = document.getElementById('home_page_header_section');
        if (window.scrollY > 5) {
            //header_elem.style.backgroundColor = 'rgba(127,0,127,.7)';
        }
        else {
            header_elem.style.backgroundColor = 'transparent';

        }
    });

    $('.datepicker').datepicker();


    $("#feel-good").hover(
  function () {
    //$(".nub-transition").slideDown('medium');   
    // $(".nub-transition").css("left","266px");
    //  $(".nub-transition").show();
     $('#feel-good .category_search_pref_list').slideDown('medium');
  }, 
  function () {
    
     $('#feel-good .category_search_pref_list').slideUp('medium');
     //$(".nub-transition").slideUp('medium');
    //  $(".nub-transition").hide();
  }
);
    $("#restore").hover(
  function () {
    //$(".nub-transition").slideDown('medium');
    // $(".nub-transition").css("left","535px");
    // $(".nub-transition").show();
     $('#restore .category_search_pref_list').slideDown('medium');
  }, 
  function () {
     $('#restore .category_search_pref_list').slideUp('medium');
     //$(".nub-transition").slideUp('medium');
    //  $(".nub-transition").hide();
  }
);
    $("#fitness").hover(
  function () {
    //$(".nub-transition").slideDown('medium');
    // $(".nub-transition").css("left","800px");
    // $(".nub-transition").show();
     $('#fitness .category_search_pref_list').slideDown('medium');
  }, 
  function () {
     $('#fitness .category_search_pref_list').slideUp('medium');
     //$(".nub-transition").slideUp('medium');
    //  $(".nub-transition").hide();
  }
);
    $("#travel-well").hover(
  function () {
    //$(".nub-transition").slideDown('medium');
    // $(".nub-transition").css("left","1060px");
    // $(".nub-transition").show();
     $('#travel-well .category_search_pref_list').slideDown('medium');
  }, 
  function () {
     $('#travel-well .category_search_pref_list').slideUp('medium');
    // $(".nub-transition").slideUp('medium');
    //    $(".nub-transition").hide();
  }
);


$(".login-div").hover(
  function () {
     $('#account-setting-slider').slideDown(300);
  }, 
  function () {
     $('#account-setting-slider').slideUp(300);
  }
);



});

function __hp_local_update_search_fields(src, dest1, dest2) {
    var search_value = document.getElementById(src).value;

    document.getElementById(dest1).value = search_value;
    if (dest2 != undefined)
        document.getElementById(dest2).value = search_value;
}

function hp_clear_views() {
    document.getElementById('home_page_search_view').style.display = 'none';
    document.getElementById('home_page_default_view').style.display = 'none';
    document.getElementById('home_page_wishlist_view').style.display = 'none';
    document.getElementById('home_page_offer_detail_view').style.display = 'none';
   // document.getElementById('home_page_header_section').classList.remove('zen_color_frames_anim');

    window.scroll(0,0);
    //
}
function hp_show_default_view() {
    hp_clear_views();
    document.getElementById('home_page_default_view').style.display = 'block';
    document.getElementById('home_page_header_section').classList.add('zen_color_frames_anim');
}

function hp_show_search_view(category) {
    hp_clear_views();
    document.getElementById('home_page_search_view').style.display = 'block';

       // var search_string = document.getElementById('home_page_zen_header_search_field').value;
    //document.getElementById('google_maps_iframe_container').setAttribute('src', local_default_gmaps_path);
    //document.getElementById('gm').setAttribute('src', 'https://maps.googleapis.com/maps/api/js?callback=initMap');
    //$.getScript( "https://maps.googleapis.com/maps/api/js?callback=initMap", function(data, textStatus, jqxhr){});
    InitGmap3();
    document.body.scrollTop = 0;
    hpso_populate_search_categories(category);
    hpso_search_offerings();
    vendor_init_amenities_tab();
    
    return false;
}

function vendor_init_amenities_tab() {
    var am_tab = document.getElementById('more_search_amenities_tab');
    am_tab.innerHTML = '';

    var amenity_list = gauth_browser_info.amenity_list;

    var amenity_html = '';
        amenity_html += "<p>Amenities</p>";

    for (var c = 0, l = amenity_list.length; c < l; c += 2)
        amenity_html += '<div class="amenity_item col-md-2"><input type="checkbox" id="vendor_edit_offering_amenity_item_' + c.toString() + '" />'
                + '<label for="vendor_edit_offering_amenity_item_' + c.toString() + '">' + amenity_list[c] + ' (' + amenity_list[c + 1] + ')</label></div>';
    am_tab.innerHTML = amenity_html;
}

function InitGmap3(){

var macDoList=[{lat:46.578498,lng:2.457275,data:{drive:false,zip:93290,city:"TREMBLAY-EN-FRANCE"}}];

  $("#map").gmap3({
  map:{
    options: {
      center:[46.578498,2.457275],
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      streetViewControl: true
    }
  },
  marker: {
    values: macDoList,
    cluster:{
      radius:100,
      // This style will be used for clusters with more than 0 markers
      0: {
        content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
        width: 53,
        height: 52
      },
      // This style will be used for clusters with more than 20 markers
      20: {
        content: "<div class='cluster cluster-2'>CLUSTER_COUNT</div>",
        width: 56,
        height: 55
      },
      // This style will be used for clusters with more than 50 markers
      50: {
        content: "<div class='cluster cluster-3'>CLUSTER_COUNT</div>",
        width: 66,
        height: 65
      }
    },
    options: {
      icon: new google.maps.MarkerImage("http://maps.gstatic.com/mapfiles/icon_green.png")
    },
    events:{
      mouseover: function(marker, event, context){
        $(this).gmap3(
          {clear:"overlay"},
          {
          overlay:{
            latLng: marker.getPosition(),
            options:{
              content:  "<div class='infobox'>"+
                         "<img src='images/Map-img.jpg'"+
                          "height='180' width='250' /> "+
                          "</div>",
              offset: {
                x:-46,
                y:-73
              }
            }
          }
        });
      },
      mouseout: function(){
        $(this).gmap3({clear:"overlay"});
      }
    }
  }
});
}

function hpso_populate_search_categories(category) {
    var option_html = '<option>All Categories</option>';
    for (var c = 0, l = gauth_browser_info.category_list.length; c < l; c++)
        option_html += '<option>' + gauth_browser_info.category_list[c] + '</option>';
    var select = document.getElementById('spso_category_field');
    select.innerHTML = option_html;
    if (category == undefined)
        select.value = 'All Categories';
    else
        select.value = category;
}

function hp_show_wishlist_view(event, sender) {
    if (gauth_logged_in_user_name == '')
    {
        alert('Login to use this feature');
        return;
    }
    hp_clear_views();
    document.getElementById('home_page_wishlist_view').style.display = 'block';
    hp_wishlist_show_wishlists();

    document.getElementById('home_page_wishlist_anchor').blur();
}

function hp_wishlist_show_wishlists(index) {
    var wishlist_data = hpwl_fetch_wishlist();

    var wishlist_dom = document.getElementById('wishlist_page_wishlist_tablist');
    var wishlist_html = '';

    for (var c = 0, l = wishlist_data.wishlists.length; c < l; c++) {
        var wishlist = wishlist_data.wishlists[c];
        var list_elem = '<li><a href="#" onclick="hpwl_show_wishlist(' + c.toString() + ')">' + wishlist.name + '</a></li>';
        wishlist_html += list_elem;
    }


    var new_elem = '<li><a href="#" onclick="hpwl_add_wishlist()">New List</a></li>';
    wishlist_html += new_elem;

    wishlist_dom.innerHTML = wishlist_html;

    if (wishlist_data.wishlists.length > 0)
        hpwl_show_wishlist(0);
    else
        hpwl_show_wishlist(-1);
}
function hpwl_add_wishlist() {
    var new_name = window.prompt('New Wishlist', 'Wishlist');
    if (!new_name || new_name.trim() == '')
        return;

    var wishlist = hpwl_fetch_wishlist();
    wishlist.wishlists.push({name: new_name, offerings: []});
    zen_auth_cjs.set_user_setting_info('zen_wishlists', JSON.stringify(wishlist));
}
function hpwl_show_wishlist(index) {
    var name_dom = $('#wishlist_page_wishlist_display_panel .listname_name_display')[0];
    var contents_dom = document.getElementById('wishlist_page_wishlist_contents');
    if (index == -1) {
        name_dom.innerHTML = 'none';
        contents_dom.innerHTML = '';
        // var anchor_dom = $('#wishlist_page_wishlist_tablist a')[0];
        // $(anchor_dom).tab('show');
        return;
    }
    var anchor_dom = $('#wishlist_page_wishlist_tablist a')[index];
    $(anchor_dom).tab('show');

    var wishlist_data = hpwl_fetch_wishlist();
    var wishlist = wishlist_data.wishlists[index];

    if (wishlist.offerings.length == 0) {
        contents_dom.innerHTML = '<br><br><br>&nbsp;&nbsp;&nbsp;This wishlist is empty.';
    }
    else {
        contents_dom.innerHTML = '';
        for (var c = 0, l = wishlist.offerings.length; c < l; c++) {
            hpwl_render_line_item(wishlist.offerings[c], contents_dom);
        }
    }
    name_dom.innerHTML = wishlist.name;
}
function hpwl_render_line_item(offering_id, contents_dom) {
    var temp_html = document.getElementById('wishlist_offering_line_item_template').innerHTML;
    var wrapper_div = document.createElement('div');
    wrapper_div.classList.add('wishlist_offering_line_item_wrapper');
    wrapper_div.innerHTML = temp_html;
    contents_dom.appendChild(wrapper_div);

    gauth_show_busy_icon(1);
    var packet = {
        browser_token: gauth_browser_info["browser_token"],
        doc_id: offering_id
    };
    $.post("/document/get", packet, function (data, status) {
        console.log(offering_id);
        console.log(data);
        data.doc_record.id = data.doc_record.id_document;

        var n_offer_bkg = $('.hpwl_offer_wrapper', wrapper_div)[0];
        var n_offer_type = $('.banner_offer_left_header_section', n_offer_bkg)[0];
        var n_offer_expiration = $('.banner_offer_expiration_footer', n_offer_bkg)[0];

        var n_offer_short_description = $('.banner_offer_footer_description', n_offer_bkg)[0];
        var n_offer_retail_price = $('.banner_offer_original_price', n_offer_bkg)[0];
        var n_offer_actual_price = $('.banner_offer_discount_price', n_offer_bkg)[0];

        var n_offer_location_description = $('.banner_offer_footer_country_desc', n_offer_bkg)[0];
        var n_offer_length = $('.banner_offer_footer_expires_desc', n_offer_bkg)[0];

        __search_render_offering_node(data.doc_record, n_offer_type, n_offer_expiration, n_offer_short_description,
                n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);


        var wishlist_list = $('.wishlistname_select', wrapper_div)[0];
        var wishlist_data = hpwl_fetch_wishlist();
        var wl_picker_html = '';
        for (var c = 0, l = wishlist_data.wishlists.length; c < l; c++)
            wl_picker_html += '<option value="' + c.toString() + '">' + wishlist_data.wishlists[c].name + '</option>';
        wishlist_list.innerHTML = wl_picker_html;

        gauth_show_busy_icon(-1);
    });
}

function changeSearchicon(button){
    debugger;
   if($(button).attr('aria-expanded')=="true"){
       $(button).children("span.glyphicon").removeClass("glyphicon-menu-up")
        $(button).children("span.glyphicon").addClass('glyphicon-menu-down');
         $(button).children("span.text").text("More Search Options");
        // $("body").css("overflow","visible");
   }else{
        
         $(button).children("span.glyphicon").removeClass("glyphicon-menu-down")
         $(button).children("span.glyphicon").addClass('glyphicon-menu-up');
         $(button).children("span.text").text("Less Search Options");
         $("body").css("overflow","scroll");
   }
}

function hpso_search_offerings() {
    /*
     id="" 
     id=""  
     id="" 
     id="spso_days_field"
     id="spso_guests_field"  
     id="spso_rooms_field" 
     id="spso_price_field"   
     */
    var category = '';
    var cat_picker = document.getElementById('spso_category_field');
    if (cat_picker.selectedIndex != 0)
        category = cat_picker.value;

    var destination = document.getElementById('spso_destination_field').value;
    var start_date = new Date(document.getElementById('spso_checkin_field').value);
    if (isNaN(start_date))
        start_date = new Date();
    start_date = start_date.toISOString().slice(0, 10);

    var stay_len = document.getElementById('spso_days_field').value.replace(/(^\d+)(.+$)/i, '$1');

    var search_options = {
        category: category,
        destination: destination,
        available_date: start_date,
        stay_len: stay_len
    };

    //clear search results
    document.getElementById('hidden_template_stash').appendChild(document.getElementById('hpsr_left_main_show_more_panel'));
    document.getElementById('hpsr_left_main_panel').innerHTML = '';

    gauth_show_busy_icon(1);
    $.post("/document/search_offerings", search_options, function (data, status) {
        global_last_search_results = data;
        global_last_search_index_shown = 0;

        if (data.offering_list.length == 0)
            document.getElementById('hpsr_left_main_panel').innerHTML = 'No results found.';

        hpso_search_show_next_block();

        gauth_show_busy_icon(-1);
    });
}
function hpso_search_show_next_block() {
    if (global_last_search_index_shown > global_last_search_results.offering_list.length - 1)
        return;

    for (var c = 0; c < global_last_search_result_count_per_fetch; c++) {
        hpso_search_fill_result_item(global_last_search_results.offering_list[global_last_search_index_shown].id, global_last_search_index_shown);
        global_last_search_index_shown++;

        if (global_last_search_index_shown > global_last_search_results.offering_list.length - 1)
            break;
    }

    //clear the floats
    var result_container = document.getElementById('hpsr_left_main_panel');
    result_container.appendChild(document.getElementById('hpsr_left_main_show_more_panel'));

    var more_button = document.getElementById('hpsr_left_main_show_more_panel');
    if (global_last_search_index_shown > global_last_search_results.offering_list.length - 1)
        more_button.style.display = 'none';
    else
        more_button.style.display = 'inline-block';
}
function hpso_search_fill_result_item(offer_id, index) {

    var is_odd = index % 2;
    //hpsr_result_offer_wrapper
    //
    var result_container = document.getElementById('hpsr_left_main_panel');
    var template_html = document.getElementById('search_result_item_template');
    var new_node = document.createElement('div');
    new_node.innerHTML = template_html.innerHTML;
    new_node.classList.add('hpsr_result_offer_wrapper');
    if (is_odd == 1)
        new_node.style.float = 'right';
    result_container.appendChild(new_node);

    //var w_class = '.hpsr_result_offer_wrapper_pos_' + (index + 1).toString();
    var n_offer_bkg = new_node;
    var n_offer_type = $('.banner_offer_left_header_section', n_offer_bkg)[0];
    var n_offer_expiration = $('.banner_offer_expiration_footer', n_offer_bkg)[0];

    var n_offer_short_description = $('.banner_offer_footer_description', n_offer_bkg)[0];
    var n_offer_retail_price = $('.banner_offer_original_price', n_offer_bkg)[0];
    var n_offer_actual_price = $('.banner_offer_discount_price', n_offer_bkg)[0];

    var n_offer_location_description = $('.banner_offer_footer_country_desc', n_offer_bkg)[0];
    var n_offer_length = $('.banner_offer_footer_expires_desc', n_offer_bkg)[0];

    gauth_show_busy_icon(1);
    var packet = {
        doc_id: offer_id
    };
    $.post("/document/get", packet, function (data, status) {
        gauth_show_busy_icon(-1);
        __search_render_offering_node(data.doc_record, n_offer_type, n_offer_expiration, n_offer_short_description,
                n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);
    });
}
function hpso_show_more_results() {
    hpso_search_show_next_block();
}

function hpwl_fetch_wishlist() {
    var wishlist_json = get_user_setting_info('zen_wishlists');

    if (wishlist_json == '') {
        var default_wishlist = {name: 'My Wishlist', offerings: []};
        var default_wishlist_data = {wishlists: [default_wishlist]};
        wishlist_json = JSON.stringify(default_wishlist_data);
        zen_auth_cjs.set_user_setting_info('zen_wishlists', wishlist_json);
    }

    var wishlist = JSON.parse(wishlist_json);
    return wishlist;
}

function wishlist_add_offering_to_list() {
    var offer_id = document.getElementById('wishlist_add_offer_id').value;
    var wishlist_data = hpwl_fetch_wishlist();
    var wishlist_index = document.getElementById('wishlist_add_offer_wishlist_list').value;

    if ($.inArray(offer_id, wishlist_data.wishlists[wishlist_index].offerings) != -1) {
        alert('This list already contains this offer');
        return;
    }
    wishlist_data.wishlists[wishlist_index].offerings.push(offer_id);
    zen_auth_cjs.set_user_setting_info('zen_wishlists', JSON.stringify(wishlist_data));

    $('#wishlist_add_offering_dialog').modal('hide');
}

function hp_show_offer_detail_view(offer_data, offer_content) {
    hp_clear_views();
    document.getElementById('home_page_offer_detail_view').style.display = 'block';
    var bkg_image = offer_data.promotional_image;
    if (bkg_image == '' || bkg_image == undefined)
        bkg_image = 'none';
    else
        bkg_image = 'url(' + encodeURI(bkg_image) + ')';
    // document.getElementById('offer_detail_top_picture_panel').style.backgroundImage = bkg_image;
    document.getElementById('offer_detail_headline_panel').innerHTML = __offer_detail_get_headline(offer_content);


    offer_data.content = offer_data.data_init;
    var doc_data = zen_process_doc_data(offer_data);

    var whatwehave_ul = document.getElementById('offer_detail_whatwehave_list');
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');
    var have_html = '';
    for (var c = 0, l = array.length; c < l; c++)
        have_html += '<li>' + array[c] + '</li>';
    whatwehave_ul.innerHTML = have_html;

    //  offer_detail_whatwezen_list
    var whatwezenul = document.getElementById('offer_detail_whatwezen_list');
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');
    var have_html = '';
    for (var c = 0, l = array.length; c < l; c++)
        have_html += '<li>' + array[c] + '</li>';
    whatwezenul.innerHTML = have_html;

    // amenity list
    var amenities_display = document.getElementById('offer_detail_amenities_display');
    var array = offer_data.amenity_list.split(',');
    var have_html = '';
    for (var c = 0, l = array.length; c < l; c++) {
        have_html += '<div class="offer_detail_amenity_item"><span class="offer_detail_amenity_icon"></span>' 
                + gauth_browser_info.amenity_list[array[c]] + '</div>';
      //                              <div class="offer_detail_amenity_item">
       //                         <span class="offer_detail_amenity_icon"></span>
      //                          Amenity 1
       //                     </div>
    }
    amenities_display.innerHTML = have_html;



   // document.getElementById('offer_detail_google_maps_iframe_container').setAttribute('src', local_default_gmaps_path);

    document.getElementById('offer_detail_overview').innerText = offer_content['offering_overview_description'];

    $('.offer_detail_location_desc')[0].innerHTML = offer_data.city;
    $('.offer_detail_stay_length_desc')[0].innerHTML = __search_get_clean_content_value(offer_content, 'offering_length_description');
    
    document.getElementById('offer_detail_expire_panel').innerHTML =  __offer_detail_getexpiration_description(offer_data);
    
    //    offer_detail_expire_panel
    
   var featured = gauth_browser_info.offering_lists['featured'].split(',');
    for (var c = 0; c < 3; c++) {
        var rec_data = __search_local_offerings_find_offering(featured[c]);
        var w_class = '.similar_offer_wrapper_pos_' + (c + 1).toString();
        var n_offer_bkg = $(w_class)[0];
        var n_offer_type = $(w_class + ' .banner_offer_left_header_section')[0];
        var n_offer_expiration = $(w_class + ' .banner_offer_expiration_footer')[0];

        var n_offer_short_description = $(w_class + ' .banner_offer_footer_description')[0];
        var n_offer_retail_price = $(w_class + ' .banner_offer_original_price')[0];
        var n_offer_actual_price = $(w_class + ' .banner_offer_discount_price')[0];

        var n_offer_location_description = $(w_class + ' .banner_offer_footer_country_desc')[0];
        var n_offer_length = $(w_class + ' .banner_offer_footer_expires_desc')[0];

        __search_render_offering_node(rec_data, n_offer_type, n_offer_expiration, n_offer_short_description,
                n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg);
    }
}
function __offer_detail_get_headline(offer_content) {
    var short_desc = offer_content.offering_short_description;
    if (short_desc == '')
        short_desc = offer_content.offering_headline;
    if (short_desc == undefined)
        short_desc = '';
    return short_desc;
}
function __search_render_offering_node(offer_data, n_offer_type, n_offer_expiration, n_offer_short_description,
        n_offer_retail_price, n_offer_actual_price, n_offer_location_description, n_offer_length, n_offer_bkg) {
    var offer_content = JSON.parse(offer_data.data_init).document.properties;

    n_offer_short_description.innerHTML = __offer_detail_get_headline(offer_content);

    var bkg_image = offer_data.promotional_image;
    if (bkg_image == '' || bkg_image == undefined) {
        bkg_image = 'none';
        n_offer_bkg.style.background = 'rgb(200,200,200)';
    }
    else {
        bkg_image = 'url(' + encodeURI(bkg_image) + ')';
        n_offer_bkg.style.background = '';
    }
    n_offer_bkg.style.backgroundImage = bkg_image;

    var location = offer_data.city;
    if (!location)
        location = '';
    console.log(location);
    n_offer_location_description.innerHTML = location;

    n_offer_length.innerHTML = __search_get_clean_content_value(offer_content, 'offering_length_description');
    
    var retail_price = __search_get_clean_content_value(offer_content, 'offering_room_retail_cost', true);
    if (retail_price) {
        n_offer_retail_price.innerHTML = retail_price;
    } else {
        n_offer_retail_price.innerHTML = '&nbsp;';
        n_offer_retail_price.style['text-decoration'] = 'none';
    }
    var actual_price = __search_get_clean_content_value(offer_content, 'offering_room_actual_cost', true);
    if (actual_price) {
        n_offer_actual_price.innerHTML = actual_price;
    } else {
        n_offer_actual_price.innerHTML = '&nbsp;';
        n_offer_actual_price.style['text-decoration'] = 'none';
    }
    n_offer_type.innerHTML = __search_get_clean_content_value(offer_content, 'offering_type_description');


    // var time_out = time_diff_moment.format("HH");
    //  (time_diff / 24) + 1
    n_offer_expiration.innerHTML =  __offer_detail_getexpiration_description(offer_data);

    $(n_offer_bkg).find('.banner_offer_footer_book_now').on('click', function () {
        search_offerings_booking_details(offer_data, offer_content);
    });
    $(n_offer_bkg).find('.rec_offer_footer_book_now').on('click', function () {
        debugger;
        search_offerings_booking_details(offer_data, offer_content);
        openSlider();
        ApplyRatyPlugin();
    });

    var local_offer_id = offer_data.id;
    $(n_offer_bkg).find('.rec_offer_right_header_section').on('click', function () {
        wishlist_show_add_offering_dialog(local_offer_id);
        return false;
    });
    $(n_offer_bkg).find('.banner_offer_right_header_section').on('click', function () {
        wishlist_show_add_offering_dialog(local_offer_id);
        return false;
    });

    n_offer_bkg.style.display = 'block';

}
function __offer_detail_getexpiration_description(offer_data) {
    var expire_date = new Date(offer_data.date_end);
    var now = new Date();
    var time_diff = Math.trunc(((expire_date - now) / (24 * 60 * 60 * 1000)) + 0.5);    
    
    if (time_diff < 0)
        return 'OFFER EXPIRED';
    return 'OFFER ENDS IN ' + time_diff.toString() + ' DAYS'
}

$(window).load(function(){
      // $('#carousel').flexslider({
      //   animation: "slide",
      //   controlNav: false,
      //   animationLoop: false,
      //   slideshow: false,
      //   itemWidth: 210,
      //   itemMargin: 5,
      //   asNavFor: '#slider'
      // });
     
      // $('#slider').flexslider({
      //   animation: "slide",
      //   controlNav: false,
      //   animationLoop: false,
        
      //   sync: "#carousel"
      // });
    })

function openSlider(){

// $('#slider-modal').modal('show')

//        setTimeout(function(){
//         $('#slider').data('flexslider').resize();
//         $('#carousel').data('flexslider').resize();
//         $('#carousel').data('flexslider').resize();
//        }, 500);

$('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        asNavFor: '#slider'
      });
     
      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        
        sync: "#carousel"
      });
}

function openMap(){
    debugger;
   
    var macDoList=[{lat:46.578498,lng:2.457275,data:{drive:false,zip:93290,city:"TREMBLAY-EN-FRANCE"}}];

  

 $('#map-modal').modal('show');

setTimeout(function(){

    $("#map-view").gmap3({
  map:{
    options: {
      center:[46.578498,2.457275],
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      streetViewControl: true
    }
  },
  marker: {
    values: macDoList,
    cluster:{
      radius:100,
      // This style will be used for clusters with more than 0 markers
      0: {
        content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
        width: 53,
        height: 52
      },
      // This style will be used for clusters with more than 20 markers
      20: {
        content: "<div class='cluster cluster-2'>CLUSTER_COUNT</div>",
        width: 56,
        height: 55
      },
      // This style will be used for clusters with more than 50 markers
      50: {
        content: "<div class='cluster cluster-3'>CLUSTER_COUNT</div>",
        width: 66,
        height: 65
      }
    },
    options: {
      icon: new google.maps.MarkerImage("http://maps.gstatic.com/mapfiles/icon_green.png")
    }
    // events:{
    //   mouseover: function(marker, event, context){
    //     $(this).gmap3(
    //       {clear:"overlay"},
    //       {
    //       overlay:{
    //         latLng: marker.getPosition(),
    //         options:{
    //           content:  "<div class='infobox'>"+
    //                      "<img src='images/Map-img.jpg'"+
    //                       "height='180' width='250' /> "+
    //                       "</div>",
    //           offset: {
    //             x:-46,
    //             y:-73
    //           }
    //         }
    //       }
    //     });
    //   },
    //   mouseout: function(){
    //     $(this).gmap3({clear:"overlay"});
    //   }
    // }
  }
});
       
       }, 500);



}

function FillCategories(){
    $.post("/admin/fetch_settings", {}, function (data, status) {
        var categories = data.setting[15].value.split(',');
         $("#hpso_activities_field").append("<option default selected>Select Categories</option>");
        for (var i = 0; i < categories.length; i++) {
        
        $("#hpso_activities_field").append("<option>"+categories[i]+"</option>");
    };
    });    
}


function ApplyRatyPlugin() {
        var rating;
        for (var i = 0; i < $(".Rating").length; i++) {
            rating = $(".Rating").eq(i).children().first().val();
            $(".Rating").eq(i).raty({ readOnly: true, score: parseFloat(rating), path: 'images/Raty/img' });
        }
    }