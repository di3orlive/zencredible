//regular use model/global memory
var gauth_book_list = [];
var gauth_book_member_list = [];
var gauth_browser_info = {};
var gauth_logged_in_user_name = '';

var gauth_vendor_user_profile = {};

var zen_auth_cjs = {};

$().ready(function () {
    require(['/scripts/zenauth.js'], function (zenauth) {
        var new_div = document.body.appendChild(document.createElement('div'));
        $(new_div).load('/userauthtemplates.html');
        zen_auth_cjs = zenauth.authengine();
        zen_auth_cjs.init_browser_token().then(function (data) {
            __gauth_success_login(data);
        });
    });
});
function gauth_init_browser_token() {        
    zen_auth_cjs.init_browser_token().then(function (data) {
            __gauth_success_login(data);
        });
}

function gauth_show_error_message(msg_name) {
    //  gauth_show_msg_dialog(resource_manager_obj.error_msg(msg_name));
    zen_auth_cjs.show_msg_dialog(msg_name);
}
function gauth_show_busy_icon(cnt) {
    zen_auth_cjs.show_busy_icon(cnt);
}
function get_user_setting_info(key) {
    return zen_auth_cjs.get_user_setting_info(key);
}
function gauth_authorize_user() {
    return zencommon_authorize_user();
}

function local_format_date(d_string) {
    if (d_string == null)
        return '';
    if (d_string == '')
        return '';
    var d_value = moment(d_string).format('MM/DD/YYYY');
    if (d_value == '01/01/1900')
        return "";
    return d_value;
}
function zencommon_authorize_user() {
    var email = document.getElementById('gauth_logon_email').value.trim();
    var password = (document.getElementById('gauth_logon_password').value.trim());
    document.getElementById('gauth_logon_password').value = '';
    zen_auth_cjs.authorize_user({email: email, password: b64_sha256(password)}, true).then(function (data) {
        __gauth_success_login(data);
        window.location.href = "http://" + window.location.hostname + ":" + window.location.port + "/Dashboard.html";
    });

    return false;
}
function zencommon_logout() {
    zen_auth_cjs.logout();
    window.location.href = "http://" + window.location.hostname + ":" + window.location.port;
    return false;
}
function zencommon_register_new_user() {
    var email = document.getElementById('gauth_logon_register_email').value;
    var display_name = document.getElementById('gauth_logon_register_displayname').value;
    var password = document.getElementById('gauth_logon_register_password').value;
    var verify_password = document.getElementById('gauth_logon_register_verify_password').value;

    var error_message = "";
    if (email.length < 3)
        error_message = "Email invalid";
    if (password.length < 3)
        error_message = "Password invalid";
    if (password != verify_password)
        error_message = "Passwords don't match";

    if (error_message != '') {
        zen_auth_cjs.show_msg_dialog(error_message, "Failed to create user", 1);
        return;
    }
    var user_profile = {};
    var password_hash = b64_sha256(password);
    var new_user_info = {
        "browser_token": zen_auth_cjs.browser_info["browser_token"],
        "display_name": display_name,
        "private_basepath": "",
        "public_basepath": "",
        "password": password_hash,
        "email": email,
        "user_profile": JSON.stringify(gauth_vendor_user_profile),
        "allow_add": 1
    };
    gauth_show_busy_icon(1);
    $.post("/auth/user_create", new_user_info, function (data, status) {
        gauth_show_busy_icon(-1);

        var new_user_json = data["new_user_result"];
        var user_id = new_user_json["id_user"];

        if (user_id == -1) {
            zen_auth_cjs.show_msg_dialog(new_user_json["description"], "Error", 1);
            return;
        }

        $('#admin_view_add_client').modal('hide');
        document.getElementById('gauth_logon_email').value = email;
        document.getElementById('gauth_logon_password').value = password;
    });

    return false;
}

function zen_process_doc_data(doc_record) {
    var doc_data = JSON.parse(doc_record.content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;
    return doc_data;
}
function zen_get_doc_array(doc_data, name) {
    var array = doc_data.properties[name];
    if (array == undefined) {
        array = [];
    }
    return array;
}
function zencommon_login_password_keydown(e) {
    if (e.keyCode == 13)
        zencommon_authorize_user();
}

function zencommon_upload_file_to_S3(dfd, file_domNode, form_dom, file_name) {
    if (file_name == '')
        file_name = file_domNode.value.replace(/^.*[\\\/]/, '');
    if (file_domNode.value.replace(/^.*[\\\/]/, '') == "")
        return;

    //a vendor at this time only has one book
    var upload_content = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_book: zen_auth_cjs.book_list[0].id_book,
        file_path: file_name,
        public: 1
    };
    zen_auth_cjs.show_busy_icon(1);
    $.post("/book/upload_policy", upload_content,
            function (data, status) {
                // var s_form = document.getElementById('s3_file_submit_form');
                $(form_dom).attr('action', zen_auth_cjs.browser_info.profile_doc.bucket_url);
                $(form_dom).find('input[name="key"]').attr('value', data['target_file_name']);
                $(form_dom).find('input[name="signature"]').attr('value', data['signature']);
                $(form_dom).find('input[name="policy"]').attr('value', data['policy_base64']);
                $(form_dom).find('input[name="content-type"]').attr('value', data['content_type']);
                $(form_dom).find('input[name="AWSAccessKeyId"]').attr('value', data['AWSAccessKeyId']);
                $(form_dom).find('input[name="acl"]').attr('value', data['acl']);

                var vFD = new FormData(form_dom);
                var oXHR = new XMLHttpRequest();
                var full_path = zen_auth_cjs.browser_info.profile_doc.bucket_url + data['target_file_name'];
                oXHR.addEventListener('load', function () {
                    gauth_show_busy_icon(-1);
                    dfd.resolve(full_path);
                }, false);
                oXHR.addEventListener('error', function (a, b, c) {
                    dfd.reject(a);
                }, false);
                oXHR.open('POST', zen_auth_cjs.browser_info.profile_doc.bucket_url, true);
                oXHR.send(vFD);
                zen_auth_cjs.show_busy_icon(-1);
            });
}

function zencommon_upload_file(file_domNode, form_dom, id_document) {
    var dfd = $.Deferred();
    file_domNode.addEventListener('change', function (evt) {
        var file_name = 'Offer' + id_document.toString() + "_" + file_domNode.value.replace(/^.*[\\\/]/, '');
        zencommon_upload_file_to_S3(dfd, file_domNode, form_dom, file_name);
    });
    return dfd;
}
function vendor_verify_create_singleton_book(data) {
    zen_auth_cjs.vendor_verify_create_singleton_book(data);
}