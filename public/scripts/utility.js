define({
    resource_mgr: function () {
        return {
            role_name: function (role_id) {
                var list = gauth_browser_info.roles;
                for (var item in list)
                    if (list[item]['id'] == role_id)
                        return list[item]['name'];
                return "";
            },
            error_msg: function (error_name) {
                var role_labels = resource_manager_obj.get().loaded_data['errors'];
                for (var item_ctr in role_labels) {
                    var data_item = role_labels[item_ctr];
                    if (error_name == data_item[0])
                        return data_item[1];
                }
                return error_name;
            },
            msg: function (msg_name) {
                var role_labels = resource_manager_obj.get().loaded_data['messages'];
                for (var item_ctr in role_labels) {
                    var data_item = role_labels[item_ctr];
                    if (error_name == data_item[0])
                        return data_item[1];
                }
                return msg_name;
            }};
    },
    helpers: function () {
        return {
            process_header_field: function (document_packet, model_packet, field_name) {
                if (document_packet.doc_record[field_name] != undefined)
                    if (document_packet.doc_record[field_name] != model_packet.doc_record[field_name]) {
                        model_packet.doc_record[field_name] = document_packet.doc_record[field_name];
                        return true;
                    }
                return false;
            },
            change_color: function (change_type) {
                if (change_type == 'property_update'
                        || change_type == 'field_update')
                    return 'rgb(150,150,255)';
                if (change_type == 'node_add')
                    return 'rgb(50,205,50)';
                if (change_type == 'node_delete')
                    return 'red';
                return 'purple';
            },
            book_picker_init: function (folder_data, container_node, fld_wgt, base_url, pub_base_path) {
                var inner_html = '';
                for (var i = 0, l = folder_data.length; i < l; i++) {
                    var item_data = folder_data[i];
                    var path = base_url + item_data['Key'];
                    var a = path.split('/');
                    var short_name = a[a.length - 1];
                    var out_name = short_name + '\n' + path;
                    if (path.toLowerCase().substr(-4) == '.mp4') {
                        inner_html += '<div class="cdoc_public_file_picker_node_mp4"><video title="' + out_name
                                + '" src="' + path + '"  /></div>';
                    }
                    else {
                        inner_html += '<div class="cdoc_public_file_picker_node"><div><img title="' + out_name
                                + '" src="' + path + '"  /></div></div>';
                    }
                }

                container_node.innerHTML = inner_html;
                dojo.query(container_node).query('.cdoc_public_file_picker_node').on('click', function (evt) {
                    var link = dojo.query(this).query('IMG').attr('SRC')[0];
                    fld_wgt.set('value', link);
                });
                dojo.query(container_node).query('.cdoc_public_file_picker_node_mp4').on('click', function (evt) {
                    var link = dojo.query(this).query('VIDEO').attr('SRC')[0];
                    fld_wgt.set('value', link);
                });
            },
            gradient_grid_advance: function (code, field, grid, stop_index, stop_count, gradient_index, gradient_count) {
                stop_index = parseInt(stop_index);
                gradient_index = parseInt(gradient_index);
                var result = {};
                if ((code == 9) || (code == 39) || (code == 13)) {
                    if (field == 'color') {
                        result['action'] = 'edit';
                        result['row'] = gradient_index + '_' + stop_index.toString();
                        result['col'] = "5";
                    }
                    else if (field == 'stop') {
                        if (stop_index != stop_count) {
                            result['action'] = 'edit';
                            result['row'] = gradient_index + '_' + (stop_index + 1).toString();
                            result['col'] = "4";
                        }
                        else {
                            if (gradient_index < gradient_count) {
                                result['action'] = 'edit';
                                result['row'] = (gradient_index + 1).toString();
                                result['col'] = "0";
                            }
                        }
                    }
                    else if (field == 'gradient') {
                        result['action'] = 'edit';
                        result['row'] = gradient_index;
                        result['col'] = "1";
                    }
                    else if (field == 'modifier') {
                        result['action'] = 'edit';
                        result['row'] = gradient_index;
                        result['col'] = "2";
                    }
                    else if (field == 'size') {
                        result['action'] = 'edit';
                        result['row'] = gradient_index;
                        result['col'] = "3";
                    }
                    else if (field == 'position') {
                        result['action'] = 'edit';
                        result['row'] = gradient_index + '_0';
                        result['col'] = "4";
                        if (gradient_index == gradient_count) {
                            result['row'] = gradient_index;
                            result['col'] = '3';
                        }
                    }
                }

//advance down
                if (code == 40) {
                    if ((field == 'stop') || (field == 'color')) {
                        if (stop_index != stop_count) {
                            result['action'] = 'edit';
                            result['row'] = gradient_index + '_' + (stop_index + 1).toString();
                            result['col'] = "4";
                            if (field == 'stop')
                                result['col'] = "5";
                        }
                        else {
                            if (gradient_index < gradient_count) {
                                result['action'] = 'edit';
                                result['row'] = (gradient_index + 1).toString();
                                result['col'] = "0";
                            }
                        }
                    }
                    else if ((field == 'gradient') || (field == 'modifier') || (field == 'size') || (field == 'position')) {
                        result['action'] = 'edit';
                        if (gradient_index == gradient_count)
                            gradient_index = gradient_count - 1;
                        result['row'] = (gradient_index + 1).toString();
                        result['col'] = "0";
                        if (field == 'modifier')
                            result['col'] = '1';
                        if (field == 'position')
                            result['col'] = '3';
                        if (field == 'size')
                            result['col'] = '2';
                    }
                }
//advance up
                if (code == 38) {
                    if ((field == 'stop') || (field == 'color')) {
//next row if needed (make sure expanded)
                        if (stop_index != 0) {
                            result['action'] = 'edit';
                            result['row'] = gradient_index + '_' + (stop_index - 1).toString();
                            result['col'] = "4";
                            if (field == 'stop')
                                result['col'] = "5";
                        }
                        else {
                            result['action'] = 'edit';
                            result['row'] = gradient_index;
                            result['col'] = "3";
                        }
                    }
                    else if ((field == 'gradient') || (field == 'modifier') || (field == 'position') || (field == 'size')) {
                        result['action'] = 'edit';
                        if (gradient_index == 0)
                            gradient_index = 1;
                        result['row'] = (gradient_index - 1).toString();
                        result['col'] = "0";
                        if (field == 'modifier')
                            result['col'] = '1';
                        if (field == 'position')
                            result['col'] = '3';
                        if (field == 'size')
                            result['col'] = '2';
                    }
                }


//advance left
                if (code == 37) {
                    if (field == 'stop') {
                        result['action'] = 'edit';
                        result['row'] = gradient_index.toString() + '_' + stop_index.toString();
                        result['col'] = "3";
                    }
                    if (field == 'color') {
                        if (stop_index == 0) {
                            result['action'] = 'edit';
                            result['row'] = gradient_index.toString();
                            result['col'] = "2";
                        }
                        else {
                            result['action'] = 'edit';
                            result['row'] = gradient_index.toString() + '_' + (stop_index - 1).toString();
                            result['col'] = "4";
                        }
                    }
                    else if ((field == 'modifier') || (field == 'position') || (field == 'size')) {
                        result['action'] = 'edit';
                        result['row'] = gradient_index.toString();
                        result['col'] = "0";
                        if (field == 'position')
                            result['col'] = '2';
                        if (field == 'size')
                            result['col'] = '1';
                    }
                    else if (field == 'gradient') {
                        result['action'] = 'edit';
                        if (gradient_index == 0)
                            gradient_index++;
                        result['row'] = (gradient_index - 1).toString();
                        result['col'] = "0";
                    }
                }

                return result;
            },
            node_index: function (nodes, name) {
                for (var node_ctr = 0; node_ctr < nodes.length; node_ctr++)
                    if ((nodes[node_ctr] != undefined) &&
                            (nodes[node_ctr]['node_identifier'] == name))
                        return node_ctr;
                return -1;
            },
            cdoc: function (name) {
                if (name == '__anim_cdoc')
                    return __anim_cdoc;
                if (name == '__nodelist_cdoc')
                    return __nodelist_cdoc;
                if (name == '__answer_cdoc')
                    return __answer_cdoc;
                if (name == '__resultlist_cdoc')
                    return __resultlist_cdoc;
                return null;
            },
            node_change: function (sender) {
                var dom_node = (!sender.domNode) ? sender : sender.domNode;
                var doc_info = cdoc_helpers.id_dom(dom_node);
                var cdoc_collection = cdoc_helpers.cdoc(doc_info.document_var);
                cdoc_collection.node_change(doc_info.id_document);
            },
            header_change: function (sender, event) {
                var dom_node = (!sender.domNode) ? sender : sender.domNode;
                var doc_info = cdoc_helpers.id_dom(dom_node);
                var id_document = doc_info.id_document;
                if (id_document == -1)
                    return;
                var cdoc_collection = cdoc_helpers.cdoc(doc_info.document_var);
                return cdoc_collection.property_change(id_document, event);
            },
            changes_to_array: function (str_in) {
                if (!str_in)
                    return [];
                if (str_in != '') {
                    str_in = str_in.replace(/(^,)|(,$)/g, "");
                    return JSON.parse('[' + str_in + ']');
                }
                return [];
            },
            gradient_mixin: function (gradient_data) {
                var bkg_img = '';
                var bkg_size = '';
                var bkg_pos = '';
                for (var g_ctr = 0, g_l = gradient_data.gradients.length; g_ctr < g_l; g_ctr++) {
                    var gradient = gradient_data.gradients[g_ctr];
                    if (g_ctr != 0) {
                        bkg_img += ',';
                        bkg_size += ',';
                        bkg_pos += ',';
                    }
                    if (gradient['gradient'] != '') {

                        bkg_img += gradient['gradient'] + '(';
                        if (gradient['modifier'] != '')
                            bkg_img += gradient['modifier'] + ',';
                        for (var s_ctr = 0, s_l = gradient.stops.length; s_ctr < s_l; s_ctr++) {
                            var cstop = gradient.stops[s_ctr];
                            if (s_ctr != 0)
                                bkg_img += ',';
                            var color = cstop['color'];
                            var stop = cstop['stop'];
                            if (color == undefined)
                                color = '';
                            if (stop == undefined)
                                stop = '';
                            if (stop != '')
                                stop = ' ' + stop;
                            bkg_img += color + stop;
                        }
                        bkg_img += ')';
                    }
                    else
                    {
                        bkg_img += 'none'; //gradient['modifier'];
                    }

                    if (gradient['position'] == undefined || gradient['position'].trim() == '')
                        bkg_pos += '0 0';
                    else
                        bkg_pos += gradient['position'];
                    if (gradient['size'] == undefined || gradient['size'].trim() == '')
                        bkg_size += '100% 100%';
                    else
                        bkg_size += gradient['size'];
                }

                var _mixin = {};
                _mixin['background_image'] = bkg_img;
                _mixin['background_size'] = bkg_size;
                _mixin['background_position'] = bkg_pos;
                _mixin['background_color'] = gradient_data['background_color'];
                return _mixin;
            },
            base_field: function (node, value, attr_name) {
                var num_attr = parseInt(node['frame_base_attr_count']);
                if (!num_attr)
                    num_attr = 0;
                //set the base frame css attr if it exists, otherwise add it if it's not empty
                var value_set = false;
                for (var attr_index = 0; attr_index < num_attr; attr_index++) {
                    var t_attr_name = node['frame_base_' + attr_index.toString() + '_attrname'];
                    if (t_attr_name != undefined)
                        if (t_attr_name.toLowerCase() == attr_name) {
                            node['frame_base_' + attr_index.toString() + '_attrvalue'] = value;
                            value_set = true;
                            break;
                        }
                }

                if ((!value_set) && (value != '')) {
                    node['frame_base_' + num_attr.toString() + '_attrname'] = attr_name;
                    node['frame_base_' + num_attr.toString() + '_attrvalue'] = value;
                    num_attr++;
                    node['frame_base_attr_count'] = num_attr;
                }
            },
            node_unique: function (new_name, nodes, index_to_exclude) {
                for (var ctr = 0; ctr < nodes.length; ctr++) {
                    if (ctr == index_to_exclude)
                        continue;
                    if (new_name == nodes[ctr]['node_identifier'])
                        return false;
                }
                return true;
            },
            raw_data: function (document_doc_init_str, change_list) {
                var base_data = {};
                if (document_doc_init_str != '')
                    base_data = JSON.parse(document_doc_init_str);
                return cdoc_helpers.merge(base_data, change_list);
            },
            change_list: function (new_node, old_node, change_type, node_name) {
                var change_result_list = [];
                var current_time = dojocomm_isostr(new Date());
                for (var field_name in new_node)
                    if (new_node.hasOwnProperty(field_name)) {
                        if (new_node[field_name] != old_node[field_name]) {
//field change (update or added)
                            var change_info = {
                                change_type: change_type,
                                node_name: node_name,
                                previous_data: old_node[field_name],
                                field_changed: field_name,
                                field_data: new_node[field_name],
                                edit_display: gauth_browser_info.profile_doc.display_name,
                                edit_time: current_time,
                                edit_email: gauth_logged_in_user_name};
                            change_result_list.push(change_info);
                        }
                    }

                for (var field_name in old_node)
                    if (old_node.hasOwnProperty(field_name)) {
                        if (new_node[field_name] == undefined) {
//field change (removed - or undone)
                            var change_info = {
                                change_type: change_type,
                                node_name: node_name,
                                previous_data: undefined,
                                field_changed: field_name,
                                field_data: old_node[field_name],
                                edit_display: gauth_browser_info.profile_doc.display_name,
                                edit_time: current_time,
                                edit_email: gauth_logged_in_user_name};
                            change_result_list.push(change_info);
                        }
                    }

                return change_result_list;
            },
            bkg: function (node_data) {
                var css_result = 'background:none;';
                var url = node_data['background_image'];
                if (url == undefined)
                    url = '';
                if (url != '') {
                    if (url.substring(0, 4).toLowerCase() == 'http')
                        url = 'url(' + url + ')';
                    if (url.substring(0, 2).toLowerCase() == '//')
                        url = 'url(' + url + ')';
                    css_result += cdoc_helpers.dim('background-image', url);
                }
                else
                    css_result += 'background-image:none;';
                css_result += cdoc_helpers.dim('background-position', node_data['background_position']);
                css_result += cdoc_helpers.dim('background-origin', node_data['background_origin']);
                css_result += cdoc_helpers.dim('background-repeat', node_data['background_repeat']);
                css_result += cdoc_helpers.dim('background-clip', node_data['background_clip']);
                css_result += cdoc_helpers.dim('background-size', node_data['background_size']);
                css_result += cdoc_helpers.dim('background-color', node_data['background_color']);
                return css_result;
            },
            expando: function (node, expando_dictionary) {
                var children = node.getChildren();
                for (var ctr = 0; ctr < children.length; ctr++)
                    cdoc_helpers.expando(children[ctr], expando_dictionary);
                if (node.isExpanded)
                    expando_dictionary[node.item['id']] = 1;
                else
                    expando_dictionary[node.item['id']] = 0;
            },
            dim: function (key, dim) {
                if (dim == undefined)
                    return '';
                if (dim == '')
                    return '';
                if (parseInt(dim).toString() == dim.toString())
                    dim += 'px';
                return key + ':' + dim + ';';
            },
            doc_change_list: function (new_document_data, previous_document_data) {
                var old_nodes = previous_document_data.nodes;
                var new_nodes = new_document_data.nodes;
                var old_ids = __dojocomm_get_string_array(old_nodes, 'node_identifier');
                var new_ids = __dojocomm_get_string_array(new_nodes, 'node_identifier');
                var current_time = dojocomm_isostr(new Date());
                //check for node adds and updates
                var change_result_list = [];
                for (var new_index = 0; new_index < new_ids.length; new_index++) {
                    var old_index = old_ids.indexOf(new_ids[new_index]);
                    if (old_index == -1) {
// add  new_ids[new_index]
                        var node_add_packet = {change_type: 'node_add',
                            node_name: new_ids[new_index],
                            add_node_data: JSON.stringify(new_nodes[new_index]),
                            edit_display: gauth_browser_info.profile_doc.display_name,
                            edit_time: current_time,
                            edit_email: gauth_logged_in_user_name};
                        change_result_list.push(node_add_packet);
                    } else // update
                    {
                        var node_changes = cdoc_helpers.change_list(
                                new_nodes[new_index],
                                old_nodes[old_index],
                                'field_update',
                                new_ids[new_index]);
                        for (; node_changes.length > 0; )
                            change_result_list.push(node_changes.splice(0, 1)[0]);
                    }

                }

//check for node deletes
                for (var old_index = 0; old_index < old_ids.length; old_index++) {
                    var new_index = new_ids.indexOf(old_ids[old_index]);
                    if (new_index == -1) {
// delete  old_ids[new_index]
                        var change_info = {
                            change_type: 'node_delete',
                            node_name: old_ids[old_index],
                            previous_data: JSON.stringify(old_nodes[old_index]),
                            edit_display: gauth_browser_info.profile_doc.display_name,
                            edit_time: current_time,
                            edit_email: gauth_logged_in_user_name
                        };
                        change_result_list.push(change_info);
                    }
                }

//check properties for field changes
                var properties_changes = cdoc_helpers.change_list(
                        new_document_data.properties,
                        previous_document_data.properties,
                        'property_update',
                        'property');
                for (; properties_changes.length > 0; )
                    change_result_list.push(properties_changes.splice(0, 1)[0]);
                return change_result_list;
            },
            id_dom: function (dom_child_node) {
                var container_dom_node = dojo.query(dom_child_node).closest('.cdoc_parent_container')[0];
                if (container_dom_node)
                    return {
                        id_document: container_dom_node.attributes['id_doc'].value,
                        document_var: container_dom_node.attributes['document_var'].value
                    };
                return {id_document: -1, document_var: ''};
            },
            merge: function (base_document, change_list) {
                var base_nodes = base_document.document.nodes;
                for (var change_ctr = 0; change_ctr < change_list.length; change_ctr++) {
                    var change_info = change_list[change_ctr];
                    var change_type = change_info['change_type'];
                    var node_name = change_info['node_name'];
                    if (change_type == "node_add") {
                        if (cdoc_helpers.node_unique(node_name, base_nodes)) {
                            var node_data = JSON.parse(change_info['add_node_data']);
                            base_nodes.push(node_data);
                        }
                    }
                    else if (change_type == "field_update") {
                        var existing_index = cdoc_helpers.node_index(base_nodes, node_name);
                        if (existing_index != -1) {
                            var field_changed = change_info['field_changed'];
                            var field_data = change_info['field_data'];
                            base_nodes[existing_index][field_changed] = field_data;
                        }
                    }
                    else if (change_type == "node_delete") {
                        var existing_index = cdoc_helpers.node_index(base_nodes, node_name);
                        if (existing_index != -1)
                            base_nodes.splice(existing_index, 1);
                    }
                    else if (change_type == "property_update") {
                        var field_changed = change_info['field_changed'];
                        var field_data = change_info['field_data'];
                        base_document.document.properties[field_changed] = field_data;
                    }
                }

                return base_document;
            }
        }
    },
    log: function () {
        return {
            props: {
                //logging:   0 - none; 1 - console; 2 - cdoc_log.props.history
                state: 0,
                //log level - 1 - public events only, 2 - internal and external events
                level: 2,
                history: [],
                prefix: 'ACTION LOG> ',
                css: 'background: #222; color: #bada55'
            }
        };
    }
});
//# sourceURL=utility.js