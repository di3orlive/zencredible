var global_admin_settings_list = {};

function admin_fetch_users() {
    var vendor_filter = 0;
    if (document.getElementById('admin_client_only_option').checked == 1)
        vendor_filter = 1;
    if (document.getElementById('admin_vendor_only_option').checked == 1)
        vendor_filter = 2;

    var text_filter = document.getElementById('admin_users_filter').value;
    var row_limit = document.getElementById('admin_users_fetch_limit').value;
    var filter_options = {
        vendor_filter: vendor_filter,
        text_filter: text_filter,
        row_limit: row_limit
    };

    var table = document.getElementById('admin_user_list_table');
    table.innerHTML = '';


    $.post("/admin/user_list", filter_options, function (data, status) {
        if (data.success <= 0) {
            alert('user list fetch failed');
            return;
        }
        var new_row = table.insertRow(-1);
        new_row.innerHTML = '<th>Email</th><th>Display Name</th><th>Base Path</th><th>Created</th><th>Vendor</th>';

        for (var c in data.user_list) {
            var new_row = table.insertRow(-1);
            var inner_html = '';
            var d_row = data.user_list[c];

            inner_html += '<td>' + d_row.email + '</td>';
            inner_html += '<td>' + d_row.display_name + '</td>';
            inner_html += '<td>' + d_row.public_basepath + '</td>';
            inner_html += '<td>' + moment(d_row.date_create).format('MM/DD/YYYY') + '</td>';
            inner_html += '<td><input class="vendoradmincheckbox" data_id=' + d_row.id.toString() + ' type="checkbox" ' + (d_row.vendor_flag == 1 ? 'checked' : '') + ' /></td>';


            new_row.innerHTML = inner_html;
        }

        $('#admin_user_list_table input[type=checkbox]').on('click', function (event) {
            var new_value = event.currentTarget.checked ? 1 : 0;
            var id = event.currentTarget.getAttribute('data_id');

            update_user_vendor_status(id, new_value);
        });
    });
}

function update_user_vendor_status(id, vendor) {
    var browser_token_info = {
        id: id,
        vendor: vendor
    };
    $.post("/admin/set_user_vendor", browser_token_info, function (data, status) {
    });
}

function admin_fetch_settings() {
    //   hp_bkg_display_time
//  hp_bkg_fade_time
//  hp_bkg_color_list
//  hp_bkg_image_list

    $.post("/admin/fetch_settings", {}, function (data, status) {
        for (var c in data.setting)
            global_admin_settings_list[data.setting[c].name] = data.setting[c].value;

        admin_populate_hp_bkg_image();
        admin_populate_hp_feature_lists();
        admin_landing_page_header_css_generate(true);
        $('#admin_categories_list_textarea').val(global_admin_settings_list.categories_list);
        $('#admin_amenities_list_textarea').val(global_admin_settings_list.amenities_list);
        
    });
}
function admin_save_setting(name, value) {
    if (global_admin_settings_list[name] != value) {
        global_admin_settings_list[name] = value;
        var packet = {name: name, value: value};
        $.post("/admin/save_setting", packet, function (data, status) {
        });
    }
}
function admin_populate_hp_bkg_image() {
    $('#admin_landing_page_bkg_display_time').val(global_admin_settings_list.hp_bkg_display_time);
    $('#admin_landing_page_bkg_fade_time').val(global_admin_settings_list.hp_bkg_fade_time);
    $('#admin_landing_page_bkg_color_list').val(global_admin_settings_list.hp_bkg_color_list);
    $('#admin_landing_page_image_list').val(global_admin_settings_list.hp_bkg_image_list);
}
function admin_landing_page_header_css_generate(dontsave) {
    admin_save_setting('hp_bkg_display_time', $('#admin_landing_page_bkg_display_time').val());
    admin_save_setting('hp_bkg_fade_time', $('#admin_landing_page_bkg_fade_time').val());
    admin_save_setting('hp_bkg_color_list', $('#admin_landing_page_bkg_color_list').val());
    admin_save_setting('hp_bkg_image_list', $('#admin_landing_page_image_list').val());

    var new_css = __admin_landing_page_generate_css_string();
    document.getElementById('landing_page_customized_css_test').innerHTML = new_css;

    if (dontsave != true)
        __admin_post_landing_css_S3(new_css);
}

function __admin_post_landing_css_S3(css_to_post) {
    var packet = {file_path: 'zen_landing_page_css.css'};
    $.post("/admin/post_s3_blob", packet, function (data, status) {
        // var s_form = document.getElementById('s3_file_submit_form');
        $('#s3_text_submit_form').attr('action', data.bucket_url);
        $('#s3_text_submit_form input[name="key"]').attr('value', data['target_file_name']);
        $('#s3_text_submit_form input[name="signature"]').attr('value', data['signature']);
        $('#s3_text_submit_form input[name="policy"]').attr('value', data['policy_base64']);
        $('#s3_text_submit_form input[name="content-type"]').attr('value', data['content_type']);
        $('#s3_text_submit_form input[name="AWSAccessKeyId"]').attr('value', data['AWSAccessKeyId']);
        $('#s3_text_submit_form input[name="acl"]').attr('value', data['acl']);
        $('#text_submission_upload').val(css_to_post);

        var form_dom = $('#s3_text_submit_form')[0];
        var vFD = new FormData(form_dom);
        var oXHR = new XMLHttpRequest();
        var full_path = data.bucket_url + data['target_file_name'];
        /*
         oXHR.addEventListener('load', function () {
         gauth_show_busy_icon(-1);
         $(field_to_output_tag).val(full_path);
         callback();
         }, false);
         oXHR.addEventListener('error', function (a, b, c) {
         alert('error');
         }, false);
         */
        oXHR.open('POST', data.bucket_url, true);
        oXHR.send(vFD);
    });
}
function __admin_landing_page_generate_css_string_old() {
    var color_list = global_admin_settings_list.hp_bkg_color_list.split(',');
    var img_list = global_admin_settings_list.hp_bkg_image_list.split(',');
    var display_time = Number(global_admin_settings_list.hp_bkg_display_time);
    var fade_time = Number(global_admin_settings_list.hp_bkg_fade_time);
    var fade_percent = fade_time * 100.0 / (display_time * 2);
    //pad color list
    var backgrounds_to_show = [];
    var odd_array = [];
    var even_array = [];

    for (var c in img_list) {
        var img_path = img_list[c];
        var color = color_list[c];
        if (color == undefined)
            color = color_list[0];
        if (img_path != undefined && img_path != '')
            backgrounds_to_show.push({color: color, img_path: img_path});
    }

    //stack the backgrounds into 2 sets (show each twice for to comp for an odd # of images)
    var odd_array = [];
    var even_array = [];
    var bkg_count = backgrounds_to_show.length;
    for (var c = 0; c < (bkg_count * 2); c++)
        if (c % 2 == 0)
            even_array.push(c % bkg_count);
        else
            odd_array.push(c % bkg_count);

    var alt_base_frame = "0% {opacity:0;";
    alt_base_frame += "background-image:url(" + backgrounds_to_show[odd_array[0]].img_path + "); }\n";

    var frame_percent = 100.0 / (bkg_count * 2);
    for (var f = 0, e_l = odd_array.length; f < e_l; f++) {
        //picture_setup
        var setup_pos = ((f * 2) + 1) * frame_percent;
        var pre_frame = (setup_pos - fade_percent).toString() + "% {";
        pre_frame += "background-image:url(" + backgrounds_to_show[odd_array[f]].img_path + ");";
        pre_frame += "opacity:0;}\n";

        var main_frame = (setup_pos).toString() + "% {";
        main_frame += "background-image:url(" + backgrounds_to_show[odd_array[f]].img_path + ");";
        main_frame += "opacity:1;}\n";

        var fade_time = setup_pos + frame_percent;
        var pre_fade_time = fade_time - fade_percent;
        var pre_fade_frame = (pre_fade_time).toString() + "% {";
        pre_fade_frame += "background-image:url(" + backgrounds_to_show[odd_array[f]].img_path + ");";
        pre_fade_frame += "opacity:1;}\n";

        var fade_frame = (fade_time).toString() + "% {";
        fade_frame += "background-image:url(" + backgrounds_to_show[odd_array[f]].img_path + ");";
        fade_frame += "opacity:0;}\n";

        alt_base_frame +=
                pre_frame +
                main_frame + pre_fade_frame + fade_frame;
    }

    // console.log(even_base_frame);
    var even_class_keyframes_name = 'zen_even_frames_keyframes';
    //double display run time
    var even_class_anim_str = even_class_keyframes_name + ' ' + (display_time / 500.0).toString() + 's infinite';
    var even_frames_class = '.zen_even_frames_anim {' +
            '-webkit-animation:' + even_class_anim_str + ';animation: ' + even_class_anim_str + ';}\n';
    var web_kit_frames = '@-webkit-keyframes ' + even_class_keyframes_name + '\n{';
    web_kit_frames += alt_base_frame + '\n}\n';
    var html5_frames = '@keyframes ' + even_class_keyframes_name + '\n{';
    html5_frames += alt_base_frame + '\n}\n';

    var even_css = web_kit_frames + html5_frames + even_frames_class;
    // console.log(even_css);


//odd is the left side, 1 is first if 1,2,3, 0 (even) is firt if 0,1,2,..
    var primary_base_frame = "0% {opacity:1;";
    primary_base_frame += "background-image:url(" + backgrounds_to_show[even_array[0]].img_path + "); }\n";

    var frame_percent = 100.0 / (bkg_count * 2);
    for (var f = 0, e_l = even_array.length; f < e_l; f++) {
        //create pre transition frame
        var next_frame_start = ((f * 2) + 1) * frame_percent;
        var pre_frame_pos = next_frame_start - fade_percent;

        var pre_frame = (pre_frame_pos).toString() + "% {";
        pre_frame += "background-image:url(" + backgrounds_to_show[even_array[f]].img_path + ");";
        pre_frame += "opacity:1;}\n";

        var main_frame = (next_frame_start).toString() + "% {";
        main_frame += "background-image:url(" + backgrounds_to_show[even_array[f]].img_path + ");";
        main_frame += "opacity:0;}\n";

        var setup_frame = '';
        var next_frame = '';
        if (f < e_l - 1) {
            var set_next_time = next_frame_start + frame_percent - fade_percent;
            var setup_frame = (set_next_time).toString() + "% {";
            setup_frame += "background-image:url(" + backgrounds_to_show[even_array[f + 1]].img_path + ");";
            setup_frame += "opacity:0;}\n";

            var next_time = next_frame_start + frame_percent;
            var next_frame = (next_time).toString() + "% {";
            next_frame += "background-image:url(" + backgrounds_to_show[even_array[f + 1]].img_path + ");";
            next_frame += "opacity:1;}\n";
        } else {
            var set_next_time = next_frame_start + frame_percent - fade_percent;
            var setup_frame = (set_next_time).toString() + "% {";
            setup_frame += "background-image:url(" + backgrounds_to_show[even_array[0]].img_path + ");";
            setup_frame += "opacity:0;}\n";

            var next_time = next_frame_start + frame_percent;
            var next_frame = (next_time).toString() + "% {";
            next_frame += "background-image:url(" + backgrounds_to_show[even_array[0]].img_path + ");";
            next_frame += "opacity:1;}\n";
        }

        primary_base_frame +=
                pre_frame +
                main_frame +
                setup_frame +
                next_frame;
    }
    // console.log(odd_base_frame);

    // console.log(even_base_frame);
    var odd_class_keyframes_name = 'zen_odd_frames_keyframes';
    //double display run time
    var odd_class_anim_str = odd_class_keyframes_name + ' ' + (display_time / 500.0).toString() + 's infinite';
    var odd_frames_class = '.zen_odd_frames_anim {' +
            '-webkit-animation:' + odd_class_anim_str + ';animation: ' + odd_class_anim_str + ';}\n';
    var web_kit_frames = '@-webkit-keyframes ' + odd_class_keyframes_name + '\n{';
    web_kit_frames += primary_base_frame + '\n}\n';
    var html5_frames = '@keyframes ' + odd_class_keyframes_name + '\n{';
    html5_frames += primary_base_frame + '\n}\n';

    var odd_css = web_kit_frames + html5_frames + odd_frames_class;
    //console.log(odd_css);


    var image_css = odd_css + even_css;


    var color_base_frame = "0% { color:" + backgrounds_to_show[0].color + " }\n";
    var frame_percent = 100.0 / (bkg_count * 2);
    var f_count = backgrounds_to_show.length * 2;
    for (var f = 0; f < f_count; f++) {
        //create pre transition frame
        var next_frame_start = (f + 1) * frame_percent;
        var pre_frame_pos = next_frame_start - fade_percent;
        var f_index = f % backgrounds_to_show.length;

        var pre_frame = (pre_frame_pos).toString() + "% {";
        pre_frame += "color:" + backgrounds_to_show[f_index].color + ";}\n";

        var main_frame = (next_frame_start).toString() + "% {";
        if (f_index >= (backgrounds_to_show.length - 1))
            main_frame += "color:" + backgrounds_to_show[0].color + ";}\n";
        else
            main_frame += "color:" + backgrounds_to_show[f_index + 1].color + ";}\n";


        color_base_frame +=
                pre_frame +
                main_frame;
    }

    var color_class_keyframes_name = 'zen_color_frames_keyframes';
    //double display run time
    var color_class_anim_str = color_class_keyframes_name + ' ' + (display_time / 500.0).toString() + 's infinite';
    var color_frames_class = '.zen_color_frames_anim {' +
            '-webkit-animation:' + color_class_anim_str + ';animation: ' + color_class_anim_str + ';}\n';
    var web_kit_frames = '@-webkit-keyframes ' + color_class_keyframes_name + '\n{';
    web_kit_frames += color_base_frame + '\n}\n';
    var html5_frames = '@keyframes ' + color_class_keyframes_name + '\n{';
    html5_frames += color_base_frame + '\n}\n';

    var color_css = web_kit_frames + html5_frames + color_frames_class;

    return color_css + image_css;
}
function __admin_landing_page_generate_css_string() {
    var color_list = global_admin_settings_list.hp_bkg_color_list.split(',');
    var img_list = global_admin_settings_list.hp_bkg_image_list.split(',');
    var display_time = Number(global_admin_settings_list.hp_bkg_display_time);
    var fade_time = Number(global_admin_settings_list.hp_bkg_fade_time);
    var fade_percent = fade_time * 100.0 / display_time;
    //pad color list
    var backgrounds_to_show = [];
    var odd_array = [];
    var even_array = [];

    for (var c in img_list) {
        var img_path = img_list[c];
        var color = color_list[c];
        if (color == undefined)
            color = color_list[0];
        if (img_path != undefined && img_path != '')
            backgrounds_to_show.push({color: color, img_path: img_path});
    }

    var background_anim_css = "";
    var frame_percent = 100.0 / backgrounds_to_show.length;
    for (var c = 0, l = backgrounds_to_show.length; c < l; c++) {
        var img_css = "background-image:url(" + backgrounds_to_show[c].img_path + ");";
        var bkg_base_frame = "0% {opacity:0;}\n";
        var frame_pos = c * frame_percent;

        if (c == 0)
            bkg_base_frame = "0% {opacity:1;}\n";
        else
        {
            //fade in frame
            var pre_frame = (frame_pos - fade_percent).toString() + "% {opacity:0;}\n";
            var main_frame = (frame_pos).toString() + "% {opacity:1;}\n";
            bkg_base_frame += pre_frame + main_frame;
        }

        var next_frame_pos = (c + 1) * frame_percent;
        var next_pre_frame = (next_frame_pos - fade_percent).toString() + "% {opacity:1;}\n";
        var next_main_frame = (next_frame_pos).toString() + "% {opacity:0;}\n";
        bkg_base_frame += next_pre_frame + next_main_frame;

        if (c == 0) {
            bkg_base_frame += (100.0 - fade_percent).toString() + "% {opacity:0;}\n";
            bkg_base_frame += "100% {opacity:1;}\n\n";
        }
        else if (c != backgrounds_to_show.length - 1)
            bkg_base_frame += "100% {opacity:0;}\n\n";

        var class_keyframes_name = 'zen_' + (c + 1).toString() + '_frames_keyframes';
        //double display run time
        var class_anim_str = class_keyframes_name + ' ' + (display_time / 1000.0).toString() + 's infinite';
        var frames_class = '.zen_' + (c + 1).toString() + '_frames_anim {' +
                '-webkit-animation:' + class_anim_str + ';animation: ' + class_anim_str + ';\n'
                + img_css + '}\n';
        var web_kit_frames = '@-webkit-keyframes ' + class_keyframes_name + '\n{';
        web_kit_frames += bkg_base_frame + '\n}\n';
        var html5_frames = '@keyframes ' + class_keyframes_name + '\n{';
        html5_frames += bkg_base_frame + '\n}\n';

        background_anim_css += web_kit_frames + html5_frames + frames_class;
    }


    var color_base_frame = "0% { color:" + backgrounds_to_show[0].color + " }\n";
    for (var f = 0; f < backgrounds_to_show.length; f++) {
        //create pre transition frame
        var next_frame_start = (f + 1) * frame_percent;
        var pre_frame_pos = next_frame_start - fade_percent;

        var pre_frame = (pre_frame_pos).toString() + "% {";
        pre_frame += "color:" + backgrounds_to_show[f].color + ";}\n";

        var main_frame = (next_frame_start).toString() + "% {";
        if (f == backgrounds_to_show.length - 1)
            main_frame += "color:" + backgrounds_to_show[0].color + ";}\n";
        else
            main_frame += "color:" + backgrounds_to_show[f + 1].color + ";}\n";


        color_base_frame +=
                pre_frame +
                main_frame;
    }

    var color_class_keyframes_name = 'zen_color_frames_keyframes';
    //double display run time
    var color_class_anim_str = color_class_keyframes_name + ' ' + (display_time / 1000.0).toString() + 's infinite';
    var color_frames_class = '.zen_color_frames_anim {' +
            '-webkit-animation:' + color_class_anim_str + ';animation: ' + color_class_anim_str + ';}\n';
    var web_kit_frames = '@-webkit-keyframes ' + color_class_keyframes_name + '\n{';
    web_kit_frames += color_base_frame + '\n}\n';
    var html5_frames = '@keyframes ' + color_class_keyframes_name + '\n{';
    html5_frames += color_base_frame + '\n}\n';

    var color_css = web_kit_frames + html5_frames + color_frames_class;

    return background_anim_css + color_css;
}

function admin_populate_hp_feature_lists() {
    $('#admin_featured_recommended_list').val(global_admin_settings_list.hp_list_recommended);
    $('#admin_featured_trending_list').val(global_admin_settings_list.hp_list_trending);
    $('#admin_featured_featured_list').val(global_admin_settings_list.hp_list_featured);
    $('#admin_featured_blogs_list').val(global_admin_settings_list.hp_list_blogs);
}
function admin_save_featured_lists() {
    admin_save_setting('hp_list_recommended', $('#admin_featured_recommended_list').val());
    admin_save_setting('hp_list_trending', $('#admin_featured_trending_list').val());
    admin_save_setting('hp_list_featured', $('#admin_featured_featured_list').val());
    admin_save_setting('hp_list_blogs', $('#admin_featured_blogs_list').val());
}
function admin_save_categories_list() {
    admin_save_setting('categories_list', $('#admin_categories_list_textarea').val());
}

function admin_save_amenities_list() {
    admin_save_setting('amenities_list', $('#admin_amenities_list_textarea').val());
}
$(document).ready(function () {
    admin_fetch_settings();
});
