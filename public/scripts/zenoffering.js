var resource_manager_obj;
var global_vendor_last_offerings = {};
var global_vendor_current_document_id = -1;

var global_default_document = {document: {nodes: [], properties: {}}};

var content_field_list = ['room_actual_cost',
    'offer_detail_photo_1', 'offer_detail_photo_2', 'offer_detail_photo_3', 'offer_detail_photo_4', 'offer_detail_photo_5',
    'offer_detail_photo_6', 'offer_detail_photo_7', 'offer_detail_photo_8', 'offer_detail_photo_9', 'offer_detail_photo_10'];

function __gauth_success_login(data) {
    gauth_book_list = data.book_list;
    if (zen_auth_cjs.book_list.length < 1 && gauth_logged_in_user_name != '') {
        vendor_verify_create_singleton_book(data);
        return;
    }

    if (zen_auth_cjs.logged_in_user_name != '') {
        if (zen_auth_cjs.browser_info.profile_doc.vendor_flag == 1) {
            var id_document = zen_auth_cjs.getParameterByName('offerid');
            vendor_init_category_lists();
            vendor_init_amenities_tab();
            var packet = zen_auth_cjs.list_document_default_packet();
            packet['id_document'] = id_document;
            packet['browser_token'] = zen_auth_cjs.browser_info["browser_token"];

            zen_auth_cjs.list_documents(packet).then(function (data) {
                 var addNew =  GetParameterValues('add');
                 if(!addNew)
                vendor_fill_offering_table(data, -1);
            });
        }
        else {
            document.getElementById('vendor_data_non_vendor').style.display = 'block';
        }
    }
    // else
    //     document.getElementById('vendor_data_non_vendor').style.display = 'block';
}
function vendor_fill_offering_table(data, id_doc_to_select) {
    global_vendor_last_offerings = {};
    for (var c in data.doc_list) {
        var d_row = data.doc_list[c];
        global_vendor_last_offerings[d_row.id_document] = d_row;
    }
    var id_document = zen_auth_cjs.getParameterByName('offerid');
   
    vendor_edit_offering_document(id_document);
    
    var form_dom  = $('#my-awesome-dropzone')[0];
    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    document.getElementById('file_upload_result_path').value = '';
    zencommon_upload_file(file_domNode, form_dom, id_document).then(function(full_path) {
        document.getElementById('file_upload_result_path').value = full_path;
        document.getElementById('file_upload_result_pathshort').value = 'Offer' + id_document.toString() + "_" + file_domNode.value.replace(/^.*[\\\/]/, '');
        ;
    });
}
function vendor_verify_create_singleton_book(data) {
    var book_add_info = {
        browser_token: data["browser_token"],
        book_name: 'VendorBook',
        public_basepath: '',
        private_basepath: '',
        enabled: 1,
        tags: ''
    };
    gauth_show_busy_icon(1);
    $.post("/book/create", book_add_info, function (data, status) {
        if (data['id_book'] <= 0) {
            alert('failed to create vendor book - contact support');
            return;
        }

        //login again so book list is fetched
        gauth_init_browser_token();
        gauth_show_busy_icon(-1);
    });

}

function vendor_edit_offering_document(id_document) {
    debugger;
    global_vendor_current_document_id = id_document;
    if (id_document != -100) {
debugger;
        document.getElementById('vendor_edit_offering_name').value = global_vendor_last_offerings[id_document].document_name;
        document.getElementById('vendor_offering_city').value = global_vendor_last_offerings[id_document].city;
        // document.getElementById('vendor_edit_offering_promotional_image').value = global_vendor_last_offerings[id_document].promotional_image;
        //document.getElementById('vendor_edit_offering_offering_image').value = global_vendor_last_offerings[id_document].offering_image;
        document.getElementById('vendor_edit_offering_category_1').value = global_vendor_last_offerings[id_document].category_1;
        $("#vendor_edit_offering_category_1").material_select();
        //document.getElementById('vendor_edit_offering_category_2').value = global_vendor_last_offerings[id_document].category_2;
        //document.getElementById('vendor_edit_offering_category_3').value = global_vendor_last_offerings[id_document].category_3;
        document.getElementById('vendor_edit_offering_min_stay').value = global_vendor_last_offerings[id_document].min_stay;
        document.getElementById('vendor_edit_offering_max_stay').value = global_vendor_last_offerings[id_document].max_stay;
        document.getElementById('vendor_edit_offering_id').value = id_document.toString();
        // document.getElementById('vendor_edit_offering_id_display').value = id_document.toString();

        document.getElementById('vendor_edit_offering_available_date').value = local_format_date(global_vendor_last_offerings[id_document].date_start);
        document.getElementById('vendor_edit_offering_end_date').value = local_format_date(global_vendor_last_offerings[id_document].date_end);
         
         document.getElementById('vendor_edit_offering_desc').value = global_vendor_last_offerings[id_document].description;

         $("#vendor_offering_country").val(global_vendor_last_offerings[id_document].country);
         $("#vendor_offering_country").material_select();
         // document.getElementById('vendor_offering_country').value = global_vendor_last_offerings[id_document].country;
         document.getElementById('vendor_offering_address').value = global_vendor_last_offerings[id_document].street_address;
         document.getElementById('vendor_offering_apt').value = global_vendor_last_offerings[id_document].apt;
         document.getElementById('vendor_offerin_zip_code').value = global_vendor_last_offerings[id_document].zip_code;

         document.getElementById('vendor_edit_offering_room_retail_cost').value = global_vendor_last_offerings[id_document].currency;
         $("#vendor_edit_offering_room_retail_cost").material_select();

          document.getElementById('vendor_edit_offering_Preparation_time').value = global_vendor_last_offerings[id_document].preparation_time;
          $("#vendor_edit_offering_Preparation_time").material_select();
          document.getElementById('vendor_edit_offering_distant_future').value = global_vendor_last_offerings[id_document].distant_future;
          $("#vendor_edit_offering_distant_future").material_select();

          var is_exclusive = global_vendor_last_offerings[id_document].is_exclusive;
          if(is_exclusive=="" || is_exclusive==undefined){
            $('#Unexclusive').css("background-color","#565a5c");
            $('#Unexclusive').css("color","#fff");
          }else{
            if(is_exclusive=="true"){
                $('#exclusive').css("background-color","#565a5c");
                $('#exclusive').css("color","#fff");
                $('.lbl-21').css("background-color","#565a5c");
                $('.lbl-21').css("color","#fff");
            }else{
                $('#Unexclusive').css("background-color","#565a5c");
                $('#Unexclusive').css("color","#fff");
                $('.lbl-17').css("background-color","#565a5c");
                $('.lbl-17').css("color","#fff");
            }
          }
        // document.getElementById('vendor_edit_offering_status').checked = (global_vendor_last_offerings[id_document].status == 1);

        var doc_data = JSON.parse(global_vendor_last_offerings[id_document].content).document;
        if (doc_data == undefined)
            doc_data = global_default_document.document;
        vendor_update_offer_preview_display(global_vendor_last_offerings[id_document], doc_data);

        for (var c = 0, l = content_field_list.length; c < l; c++)
        {
            var key_name = content_field_list[c];
            var data = doc_data.properties['offering_' + key_name];
            if (data == undefined)
                data = '';

            if (!key_name.startsWith("offer_detail_photo"))
                document.getElementById('vendor_edit_offering_' + key_name).value = data;
        }

        //vendor_whatwehave_fill_tab_list(doc_data);
        var array = zen_get_doc_array(doc_data, 'whatwezenlist');
        document.getElementById('vendor_offering_whatwezen_textbox').value = array[0];
        // vendor_whatwezen_fill_tab_list(doc_data);

        vendor_offering_amenity_fill_tab(global_vendor_last_offerings[id_document].amenity_list);
        $('.mark-active').addClass('active')
        //$('#vend$('.mark-active').addClass('active')or_edit_offering_view').modal('show');

    } else
        document.getElementById('vendor_edit_offering_id').value = id_document.toString();

}

function vendor_whatwehave_fill_tab_list(doc_data) {
    var list = document.getElementById('vendor_offering_whatwehave_list');
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');


    list.innerHTML = '';

    var list_html = '';
    for (var c = 0, l = array.length; c < l; c++) {
        var new_item = '<a href="#" index="' + c.toString() + '" class="list-group-item">' + array[c] + '</a>';
        list_html += new_item;
    }
    list.innerHTML = list_html;

    $('#vendor_offering_whatwehave_list a').on('click', function () {
        $('#vendor_offering_whatwehave_textbox').val($(this).text());
        $('#vendor_offering_whatwehave_index').val($(this).attr('index'));
    });
}
function vendor_offering_whatwehave_saveitem() {
    var index = $('#vendor_offering_whatwehave_index').val();

    var item_text = $('#vendor_offering_whatwehave_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array[index] = item_text;
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
}
function vendor_offering_whatwehave_removeitem() {
    var index = $('#vendor_offering_whatwehave_index').val();

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwehavelist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array.splice(index, 1);
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
    $('#vendor_offering_whatwehave_index').val(-1);
}
function vendor_offering_whatwehave_upitem() {
    var index = Number($('#vendor_offering_whatwehave_index').val());

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwehavelist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 1 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index - 1];
    array[Number(index) - 1] = temp;
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
    $('#vendor_offering_whatwehave_index').val(-1);

}
function vendor_offering_whatwehave_downitem() {
    var index = Number($('#vendor_offering_whatwehave_index').val());

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');

    if (index < 0 || index >= array.length - 1) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index + 1];
    array[Number(index) + 1] = temp;
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
    $('#vendor_offering_whatwehave_index').val(-1);

}
function vendor_offering_whatwehave_additem() {
    var item_text = $('#vendor_offering_whatwehave_textbox').val();

    var item_text = $('#vendor_offering_whatwehave_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwehavelist'];
    if (array == undefined) {
        array = [];
    }

    array.push(item_text);
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
}

function vendor_whatwezen_fill_tab_list(doc_data) {
    var list = document.getElementById('vendor_offering_whatwezen_list');
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');


    list.innerHTML = '';

    var list_html = '';
    for (var c = 0, l = array.length; c < l; c++) {
        var new_item = '<a href="#" index="' + c.toString() + '" class="list-group-item">' + array[c] + '</a>';
        list_html += new_item;
    }
    list.innerHTML = list_html;

    $('#vendor_offering_whatwezen_list a').on('click', function () {
        $('#vendor_offering_whatwezen_textbox').val($(this).text());
        $('#vendor_offering_whatwezen_index').val($(this).attr('index'));
    });
}
function vendor_offering_whatwezen_saveitem() {
    //var index = $('#vendor_offering_whatwezen_index').val();
    var item_text = $('#vendor_offering_whatwezen_textbox').val();
    if (item_text.length < 1) {
        //alert('Item must have some text');
        return;
    }

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');

    // if (index < 0 || index >= array.length) {
    //     alert('index out of bounds');
    //     return;
    // }

    array[0] = item_text;
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    //vendor_whatwezen_fill_tab_list(doc_data);
}
function vendor_offering_whatwezen_removeitem() {
    var index = $('#vendor_offering_whatwezen_index').val();

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwezenlist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array.splice(index, 1);
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
    $('#vendor_offering_whatwezen_index').val(-1);
}
function vendor_offering_whatwezen_upitem() {
    var index = Number($('#vendor_offering_whatwezen_index').val());

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwezenlist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 1 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index - 1];
    array[Number(index) - 1] = temp;
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
    $('#vendor_offering_whatwezen_index').val(-1);

}
function vendor_offering_whatwezen_downitem() {
    var index = Number($('#vendor_offering_whatwezen_index').val());

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');

    if (index < 0 || index >= array.length - 1) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index + 1];
    array[Number(index) + 1] = temp;
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
    $('#vendor_offering_whatwezen_index').val(-1);

}
function vendor_offering_whatwezen_additem() {
    var item_text = $('#vendor_offering_whatwezen_textbox').val();

    var item_text = $('#vendor_offering_whatwezen_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwezenlist'];
    if (array == undefined) {
        array = [];
    }

    array.push(item_text);
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
}

function vendor_offering_amenity_fill_tab(list_data) {
    var amenity_list = zen_auth_cjs.browser_info.amenity_list;
    if (list_data == undefined)
        list_data = '';
    var selected_list = list_data.split(',');

    for (var c = 0, l = amenity_list.length; c < l; c += 2) {
        var id = 'vendor_edit_offering_amenity_item_' + c.toString();
        if ($.inArray(c.toString(), selected_list) != -1)
            document.getElementById(id).checked = true;
        else
            document.getElementById(id).checked = false;
    }
}

function vendor_add_offering() {
    debugger;
    var id_book = gauth_book_list[0].id_book;
    var id_doctype = "1";

    var doc_name = document.getElementById('vendor_edit_offering_name').value;
    if (doc_name == null)
        return;
    if (doc_name.length < 3) {
        alert('Name is too short');
        return;
    }

    var doc = {document:
                {
                    nodes: [],
                    properties: {}
                }
    };
    var doc_props = doc.document.properties;
    var data_init = JSON.stringify(doc);
    var doc_add_info = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_book: id_book,
        document_name: doc_name,
        public_basepath: '',
        private_basepath: '',
        id_doctype: id_doctype,
        enabled: 1,
        tags: '',
        status: 0,
        data_init: data_init
    };

    gauth_show_busy_icon(1);
    $.post("/document/create", doc_add_info, function (data, status) {
        if (data['id_result'] < 0) {
            gauth_show_error_message(data['description']);
        }
        else {
            debugger;
            var id_document = data['id_result'];
            global_vendor_current_document_id = id_document;
            document.getElementById('vendor_edit_offering_id').value = id_document;
            var packet =
                    {max_records: "50",
                        book_list: '',
                        sort_column: 'doc_edit',
                        sort_order: 'desc'
                    };

            packet['doctype'] = "1";
            packet['browser_token'] = zen_auth_cjs.browser_info["browser_token"];
            $.post("/document/list", packet, function (data, status) {
                global_vendor_last_offerings = {};
                for (var c in data.doc_list) {
                    var d_row = data.doc_list[c];
                    global_vendor_last_offerings[d_row.id_document] = d_row;
                }
                vendor_offering_save_details();
                enableContent();
                $('#Waiting').closeModal();
            });

        }

        gauth_show_busy_icon(-1);
    });


}


function vendor_offering_save_details() {
    vendor_offering_whatwezen_saveitem();
    var id_document = Number(document.getElementById('vendor_edit_offering_id').value);
    var document_name = document.getElementById('vendor_edit_offering_name').value;
    //var is_active = document.getElementById('vendor_edit_offering_status').checked;
    var date_start = document.getElementById('vendor_edit_offering_available_date').value;
    var date_end = document.getElementById('vendor_edit_offering_end_date').value;
    debugger;
    var city = document.getElementById('vendor_offering_city').value;
    //var promotional_image = document.getElementById('vendor_edit_offering_promotional_image').value;
    //var offering_image = document.getElementById('vendor_edit_offering_offering_image').value;
    var category_1 = document.getElementById('vendor_edit_offering_category_1').value;
    // var category_2 = document.getElementById('vendor_edit_offering_category_2').value;
    // var category_3 = document.getElementById('vendor_edit_offering_category_3').value;
    var min_stay = Number(document.getElementById('vendor_edit_offering_min_stay').value);
    var max_stay = Number(document.getElementById('vendor_edit_offering_max_stay').value);
    var is_exclusive = document.getElementById('vendor_edit_offering_is_exclusive').value;
    var country = $("#vendor_offering_country option:selected").val();
    var street_address = document.getElementById('vendor_offering_address').value;
    var apt = document.getElementById('vendor_offering_apt').value;
    var zip_code = document.getElementById('vendor_offerin_zip_code').value;
    var currency = $("#vendor_edit_offering_room_retail_cost option:selected").text();
    var preparation_time = $("#vendor_edit_offering_Preparation_time option:selected").val();
    var distant_future = $("#vendor_edit_offering_distant_future option:selected").val();
    debugger;
    var description = document.getElementById('vendor_edit_offering_desc').value;

    if (isNaN(min_stay))
        min_stay = 0;
    if (isNaN(max_stay))
        max_stay = 0;
    //     url: "/document/update",
    var doc_data = JSON.parse(global_vendor_last_offerings[id_document].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    for (var c = 0, l = content_field_list.length; c < l; c++) {
        if (!content_field_list[c].startsWith("offer_detail_photo"))
            doc_data.properties['offering_' + content_field_list[c]] = document.getElementById('vendor_edit_offering_' + content_field_list[c]).value;
    }
    var amenity_choices = vendor_offering_scrape_amenity_selections();

    //get change list
    var packet = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_document: id_document,
        document_name: document_name,
        city: city,
        date_start: date_start,
        date_end: date_end,
        search_tags: '',
        //status: is_active ? 1 : 0,
        //promotional_image: promotional_image,
        // offering_image: offering_image,
        amenity_list: amenity_choices,
        category_1: category_1,
        //category_2: category_2,
        // category_3: category_3,
        min_stay: min_stay,
        max_stay: max_stay,
        apt : apt,
        is_exclusive : is_exclusive,
        country : country,
        street_address : street_address,
        
        zip_code : zip_code,
        currency : currency,
        preparation_time : preparation_time,
        distant_future  : distant_future,
        description : description ,
        content: JSON.stringify({document: doc_data}),        
    };
    $.post("/document/update", packet, function (data, status) {
        //changestep(1);
    });


    vendor_update_offer_preview_display(global_vendor_last_offerings[id_document], doc_data);

}

//}

function vendor_update_offer_preview_display(offer_data, doc_data) {
    return;
    function __offer_detail_get_headline(offer_content) {
        var short_desc = offer_content.offering_short_description;
        if (short_desc == '')
            short_desc = offer_content.offering_headline;
        if (short_desc == undefined)
            short_desc = '';
        return short_desc;
    }
    function __search_get_clean_content_value(data, value_tag) {
        var value = data[value_tag];
        if (value == undefined)
            value = '';
        return value;
    }
    function __offer_detail_getexpiration_description(offer_data) {
        var expire_date = new Date(offer_data.date_end);
        var now = new Date();
        var time_diff = Math.trunc(((expire_date - now) / (24 * 60 * 60 * 1000)) + 0.5);

        if (time_diff < 0)
            return 'OFFER EXPIRED';
        return 'OFFER ENDS IN ' + time_diff.toString() + ' DAYS'
    }

    var n_offer_bkg = $('.offer_preview_content_holder')[0];
    var n_offer_type = $('.offer_preview_content_holder .rec_offer_left_header_section')[0];
    var n_offer_expiration = $('.offer_preview_content_holder .rec_offer_middle_header_section')[0];

    var n_offer_short_description = $('.offer_preview_content_holder .rec_offer_footer_description')[0];
    var n_offer_retail_price = $('.offer_preview_content_holder .rec_offer_original_price')[0];
    var n_offer_actual_price = $('.offer_preview_content_holder .rec_offer_discount_price')[0];

    var n_offer_location_description = $('.offer_preview_content_holder .rec_offer_footer_country_desc')[0];
    var n_offer_length = $('.offer_preview_content_holder .rec_offer_footer_expires_desc')[0];
    var offer_content = doc_data.properties;

    n_offer_short_description.innerHTML = __offer_detail_get_headline(offer_content);

    var bkg_image = offer_data.promotional_image;
    if (bkg_image == '' || bkg_image == undefined) {
        bkg_image = 'none';
        n_offer_bkg.style.background = 'rgb(200,200,200)';
    }
    else {
        bkg_image = 'url(' + encodeURI(bkg_image) + ')';
        n_offer_bkg.style.background = '';
    }
    n_offer_bkg.style.backgroundImage = bkg_image;

    var location = offer_data.city;
    if (location == undefined)
        location = '';
    n_offer_location_description.innerHTML = location;

    n_offer_length.innerHTML = __search_get_clean_content_value(offer_content, 'offering_length_description');
    n_offer_retail_price.innerHTML = __search_get_clean_content_value(offer_content, 'offering_room_retail_cost');
    n_offer_actual_price.innerHTML = __search_get_clean_content_value(offer_content, 'offering_room_actual_cost');
    n_offer_type.innerHTML = __search_get_clean_content_value(offer_content, 'offering_type_description');


    // var time_out = time_diff_moment.format("HH");
    //  (time_diff / 24) + 1
    n_offer_expiration.innerHTML = __offer_detail_getexpiration_description(offer_data);

    $(n_offer_bkg).find('.banner_offer_footer_book_now').on('click', function () {
        search_offerings_booking_details(offer_data, offer_content);
    });
    $(n_offer_bkg).find('.rec_offer_footer_book_now').on('click', function () {
        search_offerings_booking_details(offer_data, offer_content);
    });

    var local_offer_id = offer_data.id;
    $(n_offer_bkg).find('.rec_offer_right_header_section').on('click', function () {
        wishlist_show_add_offering_dialog(local_offer_id);
        return false;
    });
    $(n_offer_bkg).find('.banner_offer_right_header_section').on('click', function () {
        wishlist_show_add_offering_dialog(local_offer_id);
        return false;
    });

    n_offer_bkg.style.display = 'block';
}


function upload_file_for_client_to_S3(field_to_output_tag, callback, id_document) {
    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    var file_name = file_domNode.value.replace(/^.*[\\\/]/, '');
    if (file_name == "")
        return;

    var upload_content = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book,
        file_path: file_name,
        public: 1
    };
    gauth_show_busy_icon(1);
    $(field_to_output_tag).val('Uploading...');
    $.post("/book/upload_policy", upload_content,
            function (data, status) {
                // var s_form = document.getElementById('s3_file_submit_form');
                $('#s3_file_submit_form').attr('action', zen_auth_cjs.browser_info.profile_doc.bucket_url);
                $('#s3_file_submit_form input[name="key"]').attr('value', data['target_file_name']);
                $('#s3_file_submit_form input[name="signature"]').attr('value', data['signature']);
                $('#s3_file_submit_form input[name="policy"]').attr('value', data['policy_base64']);
                $('#s3_file_submit_form input[name="content-type"]').attr('value', data['content_type']);
                $('#s3_file_submit_form input[name="AWSAccessKeyId"]').attr('value', data['AWSAccessKeyId']);
                $('#s3_file_submit_form input[name="acl"]').attr('value', data['acl']);

                var form_dom = $('#s3_file_submit_form')[0];
                var vFD = new FormData(form_dom);
                var oXHR = new XMLHttpRequest();
                var full_path = zen_auth_cjs.browser_info.profile_doc.bucket_url + data['target_file_name'];
                oXHR.addEventListener('load', function () {
                    gauth_show_busy_icon(-1);
                    $(field_to_output_tag).val(full_path);
                    callback();
                }, false);
                oXHR.addEventListener('error', function (a, b, c) {
                    alert('error');
                }, false);
                oXHR.open('POST', zen_auth_cjs.browser_info.profile_doc.bucket_url, true);
                oXHR.send(vFD);
                gauth_show_busy_icon(-1);
            });
}
function vendor_offering_upload(text_box) {
    var id_document = Number(document.getElementById('vendor_edit_offering_id').value);
    var result_f = document.getElementById(text_box);
    result_f.value = '';

    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    file_domNode.value = '';
    file_domNode.onchange = function (evt) {
        upload_file_for_client_to_S3('#' + text_box, function () {
        }, id_document);
    };
    file_domNode.click();
}
function vendor_fetch_file_list() {
    var packet = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book
    };
    gauth_show_busy_icon(1);
    $.post("/book/file_list", packet, function (data, status) {
        var table = document.getElementById('vendor_offering_files_table');
        table.innerHTML = '';

        for (var c in data.dir_result) {
            var new_row = table.insertRow(-1);
            var inner_html = '';
            var d_row = data.dir_result[c];

            var file_name = d_row.Key.split('/')[3];

            inner_html += '<td>' + file_name + ' &nbsp; &nbsp; <button onclick="vendor_delete_file(\'' + file_name + '\')">Delete</button>';
            inner_html += '<br><img class="image_preview" src="' + zen_auth_cjs.browser_info.profile_doc.bucket_url + d_row.Key + '" />';
            inner_html += '</td>';

            new_row.innerHTML = inner_html;
        }
        gauth_show_busy_icon(-1);
    });

}
function vendor_delete_file(file_key) {

    var packet = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book,
        file_path: file_key,
        public: 1
    };
    gauth_show_busy_icon(1);
    $.post("/book/delete_file", packet, function (data, status) {
        vendor_fetch_file_list();
        gauth_show_busy_icon(-1);
    });



}

function vendor_init_category_lists() {
    var category_list = zen_auth_cjs.browser_info.category_list;

    var option_html = '';
    option_html += '<option>choose one</option>';
    for (var c = 0, l = category_list.length; c < l; c++)
        option_html += '<option>' + category_list[c] + '</option>';

    document.getElementById('vendor_edit_offering_category_1').innerHTML = option_html;
    // document.getElementById('vendor_edit_offering_category_2').innerHTML = option_html;
    //document.getElementById('vendor_edit_offering_category_3').innerHTML = option_html;
    $('#vendor_edit_offering_category_1').material_select();
    //$('#vendor_edit_offering_category_2').material_select();
    //$('#vendor_edit_offering_category_3').material_select();
}
function vendor_init_amenities_tab() {
    var am_tab = document.getElementById('vendor_offering_amenities_tab');
    am_tab.innerHTML = '';

    var amenity_list = zen_auth_cjs.browser_info.amenity_list;

    var amenity_html = '';
    amenity_html += '<div><h3 class="decription-head">Tell travelers about your space</h3>'
            + '<p class="decription-p">Every space on Zencredible is unique. Highlight what makes your listing welcoming so that it stands out to guests who want to stay in your area.</p>'
            + '<hr/></div><h3 class="decription-head">Common Amenities</h3>';

    for (var c = 0, l = amenity_list.length; c < l; c += 2)
        amenity_html += '<div class="amenity_item"><input type="checkbox" id="vendor_edit_offering_amenity_item_' + c.toString() + '" />'
                + '<label for="vendor_edit_offering_amenity_item_' + c.toString() + '">' + amenity_list[c] + ' (' + amenity_list[c + 1] + ')</label></div>';
    am_tab.innerHTML = amenity_html;
}
function vendor_offering_scrape_amenity_selections() {
    var amenity_list = zen_auth_cjs.browser_info.amenity_list;

    var selected_list = '';
    for (var c = 0, l = amenity_list.length; c < l; c += 2) {
        var id = 'vendor_edit_offering_amenity_item_' + c.toString();
        if (document.getElementById(id).checked)
            selected_list += c.toString() + ',';
    }

    if (selected_list != '')
        selected_list = selected_list.substring(0, selected_list.length - 1);
    return selected_list;
}
