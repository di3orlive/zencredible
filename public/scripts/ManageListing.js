var resource_manager_obj;
var global_vendor_last_offerings = {};
var global_vendor_current_document_id = -1;

var global_default_document = {document: {nodes: [], properties: {}}};

require(['/scripts/utility.js'], function (cdoc_utility) {
    resource_manager_obj = cdoc_utility.resource_mgr();
});

function __gauth_success_login(data) {
    gauth_browser_info = data;
    gauth_book_list = data.book_list;
    gauth_book_member_list = data.book_member_list;
    gauth_logged_in_user_name = data.profile_doc.email;
    if (gauth_book_list.length < 1 && gauth_logged_in_user_name != '') {
        vendor_verify_create_singleton_book(data);
        return;
    }

    if (gauth_logged_in_user_name != '') {
        $('#login_view').closeModal();
        // document.getElementById('outer_wrapper_load').style.display = 'block';
        document.getElementById('loggedin_name').innerHTML = gauth_logged_in_user_name;
        document.getElementById('loggedin_user_panel').style.display = 'inline-block';
        document.getElementById('notloggedin_user_panel').style.display = 'none';
        var usertype = gauth_browser_info.profile_doc.hc_type;
        // var c_pwd_btn = document.getElementById('gauth_change_password_btn');
        // c_pwd_btn.style.display = 'inline-block';
        var c_username_display = document.getElementById('loggedin_name');
        c_username_display.style.display = '';
        if (gauth_browser_info.profile_doc.vendor_flag == 1) {
            //document.getElementById('vendor_data_tabs').style.display = 'block';
            vendor_fetch_offerings(-1);
            // vendor_init_category_lists();
            // vendor_init_amenities_tab();
            // document.getElementById('vendor_data_non_vendor').style.display = 'none';
        }
        else {
            // document.getElementById('vendor_data_non_vendor').style.display = 'block';
        }


        if (get_user_setting_info("vendor_signed_up") == "1") {
            document.getElementById('vendor_signup_left_header').innerHTML = "Already signed up.";
            document.getElementById('vendor_signup_left_description').innerHTML = 'Awaiting approval';
            document.getElementById('vendor_signup_right_panel').style.display = 'none';
        }
    }
    //else
    // document.getElementById('vendor_data_non_vendor').style.display = 'block';
}
function vendor_verify_create_singleton_book(data) {
    var book_add_info = {
        browser_token: data["browser_token"],
        book_name: 'VendorBook',
        public_basepath: '',
        private_basepath: '',
        enabled: 1,
        tags: ''
    };
    gauth_show_busy_icon(1);
    $.post("/book/create", book_add_info, function (data, status) {
        if (data['id_book'] <= 0) {
            alert('failed to create vendor book - contact support');
            return;
        }

        //login again so book list is fetched
        gauth_init_browser_token();
        gauth_show_busy_icon(-1);
    });

}

function vendor_fetch_offerings(id_doc_to_select) {
    var packet =
            {max_records: "50",
                book_list: '',
                sort_column: 'doc_edit',
                sort_order: 'desc'
            };

    // if (document.getElementById('vendor_offerings_active_filter').checked)
    //     packet['status'] = '1';
    packet['doctype'] = "1";
    packet['browser_token'] = gauth_browser_info["browser_token"];
    gauth_show_busy_icon(1);

    $.post("/document/list", packet, function (data, status) {
        vendor_fill_offering_table(data, id_doc_to_select);
        gauth_show_busy_icon(-1);
    });
}
function vendor_fill_offering_table(data, id_doc_to_select) {
    var table = document.getElementById('vendor_offering_list_table');
    table.innerHTML = '';

    var inner_html = '';
    global_vendor_last_offerings = {};
    for (var c in data.doc_list) {

        var d_row = data.doc_list[c];

        inner_html += '<div class="row" style="padding-top:20px">' +
                '<div class="col s3">' +
                '<a href="#"><div class="photo-div"><img src="photo-icon.png" /> </div> </a>' +
                '</div>' +
                '<div class="col s6">' +
                '<label class="offer-name">' + d_row.document_name + '</label><br/>' +
                '<a href="/Offering.html?offerid=' + d_row.id_document.toString() + '" class="mange-listing-a">Manage Listing and Calendar</a>' +
                '</div>' +
                '<div class="col s3">' +
                '<a class="btn btn-host col s10" href="/Offering.html?offerid=' + d_row.id_document.toString() + '" style="margin-top: 14px;">Manage Listing</a>' +
                '</div>' +
                '</div>';
       global_vendor_last_offerings[d_row.id_document] = d_row;
    }
    table.innerHTML = inner_html;
}

function zen_add_new_listing() {
    var id_book = gauth_book_list[0].id_book;
    var id_doctype = "1";

    var doc = {document:
                {
                    nodes: [],
                    properties: {}
                }
    };
    var doc_props = doc.document.properties;
    var data_init = JSON.stringify(doc);
    var doc_add_info = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: id_book,
        document_name: "New Listing",
        public_basepath: '',
        private_basepath: '',
        id_doctype: id_doctype,
        enabled: 1,
        tags: '',
        status: 0,
        data_init: data_init
    };

    gauth_show_busy_icon(1);
    $.post("/document/create", doc_add_info, function (data, status) {
        if (data['id_result'] < 0) {
            gauth_show_error_message(data['description']);
        }
        else {
            var id_document = data['id_result'];
            window.location = "Offering.html?offerid=" + id_document.toString();
        }

        gauth_show_busy_icon(-1);
    });
    
    return false; // prevent the # in the path
}

function vendor_offering_amenity_fill_tab(list_data) {
    var amenity_list = gauth_browser_info.amenity_list;
    if (list_data == undefined)
        list_data = '';
    var selected_list = list_data.split(',');

    for (var c = 0, l = amenity_list.length; c < l; c += 2) {
        var id = 'vendor_edit_offering_amenity_item_' + c.toString();
        if ($.inArray(c.toString(), selected_list) != -1)
            document.getElementById(id).checked = true;
        else
            document.getElementById(id).checked = false;
    }
}
$(document).ready(function () {
    // $('.datepicker').datepicker({
    //     format: "mm/dd/yyyy"
    // });

});

function upload_file_for_client_to_S3(field_to_output_tag, callback, id_document) {
    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    var file_name = file_domNode.value.replace(/^.*[\\\/]/, '');
    if (file_name == "")
        return;

    var upload_content = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book,
        file_path: file_name,
        public: 1
    };
    gauth_show_busy_icon(1);
    $(field_to_output_tag).val('Uploading...');
    $.post("/book/upload_policy", upload_content,
            function (data, status) {
                // var s_form = document.getElementById('s3_file_submit_form');
                $('#s3_file_submit_form').attr('action', gauth_browser_info.profile_doc.bucket_url);
                $('#s3_file_submit_form input[name="key"]').attr('value', data['target_file_name']);
                $('#s3_file_submit_form input[name="signature"]').attr('value', data['signature']);
                $('#s3_file_submit_form input[name="policy"]').attr('value', data['policy_base64']);
                $('#s3_file_submit_form input[name="content-type"]').attr('value', data['content_type']);
                $('#s3_file_submit_form input[name="AWSAccessKeyId"]').attr('value', data['AWSAccessKeyId']);
                $('#s3_file_submit_form input[name="acl"]').attr('value', data['acl']);

                var form_dom = $('#s3_file_submit_form')[0];
                var vFD = new FormData(form_dom);
                var oXHR = new XMLHttpRequest();
                var full_path = gauth_browser_info.profile_doc.bucket_url + data['target_file_name'];
                oXHR.addEventListener('load', function () {
                    gauth_show_busy_icon(-1);
                    $(field_to_output_tag).val(full_path);
                    callback();
                }, false);
                oXHR.addEventListener('error', function (a, b, c) {
                    alert('error');
                }, false);
                oXHR.open('POST', gauth_browser_info.profile_doc.bucket_url, true);
                oXHR.send(vFD);
                gauth_show_busy_icon(-1);
            });
}
function vendor_offering_upload(text_box) {
    var id_document = Number(document.getElementById('vendor_edit_offering_id').value);
    var result_f = document.getElementById(text_box);
    result_f.value = '';

    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    file_domNode.value = '';
    file_domNode.onchange = function (evt) {
        upload_file_for_client_to_S3('#' + text_box, function () {

        }, id_document);
    };
    file_domNode.click();
}
function vendor_fetch_file_list() {
    var packet = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book
    };
    gauth_show_busy_icon(1);
    $.post("/book/file_list", packet, function (data, status) {
        var table = document.getElementById('vendor_offering_files_table');
        table.innerHTML = '';

        for (var c in data.dir_result) {
            var new_row = table.insertRow(-1);
            var inner_html = '';
            var d_row = data.dir_result[c];

            var file_name = d_row.Key.split('/')[3];

            inner_html += '<td>' + file_name + ' &nbsp; &nbsp; <button onclick="vendor_delete_file(\'' + file_name + '\')">Delete</button>';
            inner_html += '<br><img class="image_preview" src="' + gauth_browser_info.profile_doc.bucket_url + d_row.Key + '" />';
            inner_html += '</td>';

            new_row.innerHTML = inner_html;
        }
        gauth_show_busy_icon(-1);
    });

}
function vendor_delete_file(file_key) {

    var packet = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book,
        file_path: file_key,
        public: 1
    };
    gauth_show_busy_icon(1);
    $.post("/book/delete_file", packet, function (data, status) {
        vendor_fetch_file_list();
        gauth_show_busy_icon(-1);
    });



}

function vendor_init_category_lists() {
    var category_list = gauth_browser_info.category_list;

    var option_html = '';
    for (var c = 0, l = category_list.length; c < l; c++)
        option_html += '<option>' + category_list[c] + '</option>';

    // document.getElementById('vendor_edit_offering_category_1').innerHTML = option_html;
    // document.getElementById('vendor_edit_offering_category_2').innerHTML = option_html;
    // document.getElementById('vendor_edit_offering_category_3').innerHTML = option_html;
}
function vendor_init_amenities_tab() {
    var am_tab = document.getElementById('vendor_offering_amenities_tab');
    //am_tab.innerHTML = '';

    var amenity_list = gauth_browser_info.amenity_list;

    var amenity_html = '';
    for (var c = 0, l = amenity_list.length; c < l; c += 2)
        amenity_html += '<div class="amenity_item"><input type="checkbox" id="vendor_edit_offering_amenity_item_' + c.toString() + '" />'
                + '<label for="vendor_edit_offering_amenity_item_' + c.toString() + '">' + amenity_list[c] + ' (' + amenity_list[c + 1] + ')</label></div>';
    //am_tab.innerHTML = amenity_html;
}
function vendor_offering_scrape_amenity_selections() {
    var amenity_list = gauth_browser_info.amenity_list;

    // var selected_list = '';
    for (var c = 0, l = amenity_list.length; c < l; c += 2) {
        var id = 'vendor_edit_offering_amenity_item_' + c.toString();
        if (document.getElementById(id).checked)
            selected_list += c.toString() + ',';
    }

    if (selected_list != '')
        selected_list = selected_list.substring(0, selected_list.length - 1);
    return selected_list;
}

function vendor_initialize_signup_data() {
    set_user_setting_info('vendor_signed_up', "1");
    set_user_setting_info('vendor_signup_hotel_name', document.getElementById('vendor_signup_hotel_name').value);
    set_user_setting_info('vendor_signup_street_address', document.getElementById('vendor_signup_street_address').value);
    set_user_setting_info('vendor_signup_city', document.getElementById('vendor_signup_city').value);
    set_user_setting_info('vendor_signup_state', document.getElementById('vendor_signup_state').value);
    set_user_setting_info('vendor_signup_zipcode', document.getElementById('vendor_signup_zipcode').value);
    set_user_setting_info('vendor_signup_country', document.getElementById('vendor_signup_country').value);
    set_user_setting_info('vendor_signup_website', document.getElementById('vendor_signup_website').value);

    set_user_setting_info('vendor_signup_first_name', document.getElementById('vendor_signup_first_name').value);
    set_user_setting_info('vendor_signup_last_name', document.getElementById('vendor_signup_last_name').value);
    set_user_setting_info('vendor_signup_hotel_role', document.getElementById('vendor_signup_hotel_role').value);
    set_user_setting_info('vendor_signup_email', document.getElementById('vendor_signup_email').value);
    set_user_setting_info('vendor_signup_phone', document.getElementById('vendor_signup_phone').value);
    window.location = location.protocol + '//' + location.host + location.pathname;
}

function gauth_register_new_vendor() {
    //if they are logged in, use the existing account
    if (gauth_logged_in_user_name != '') {
        vendor_initialize_signup_data();
    }
    else {
        alert("Please create a client account, login with that account and return to this page.");
    }
}
