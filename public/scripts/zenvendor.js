var global_vendor_last_offerings = {};
var global_vendor_current_document_id = -1;

var global_default_document = {document: {nodes: [], properties: {}}};


var content_field_list = ['headline', 'room_actual_cost', 'room_retail_cost', 'type_description',
    'length_description', 'overview_description', 'offer_detail_photo_1', 'offer_detail_photo_2', 'offer_detail_photo_3', 'offer_detail_photo_4', 'offer_detail_photo_5',
    'offer_detail_photo_6', 'offer_detail_photo_7', 'offer_detail_photo_8', 'offer_detail_photo_9', 'offer_detail_photo_10'];

function __gauth_success_login(data) {
    gauth_browser_info = data;
    gauth_book_list = data.book_list;
    gauth_book_member_list = data.book_member_list;
    gauth_logged_in_user_name = data.profile_doc.email;
    if (gauth_book_list.length < 1 && gauth_logged_in_user_name != '') {
        zen_auth_cjs.vendor_verify_create_singleton_book(data);
        return;
    }

    if (gauth_logged_in_user_name != '') {
        $('#login_view').modal('hide');
        document.getElementById('outer_wrapper_load').style.display = 'block';
        document.getElementById('loggedin_name').innerHTML = gauth_logged_in_user_name;
        document.getElementById('loggedin_user_panel').style.display = 'inline-block';
        document.getElementById('notloggedin_user_panel').style.display = 'none';
        var usertype = gauth_browser_info.profile_doc.hc_type;
        // var c_pwd_btn = document.getElementById('gauth_change_password_btn');
        // c_pwd_btn.style.display = 'inline-block';
        var c_username_display = document.getElementById('loggedin_name');
        c_username_display.style.display = '';
        if (gauth_browser_info.profile_doc.vendor_flag == 1) {
            document.getElementById('vendor_data_tabs').style.display = 'block';
            vendor_fetch_offerings(-1);
            vendor_init_category_lists();
            vendor_init_amenities_tab();
            document.getElementById('vendor_data_non_vendor').style.display = 'none';
        }
        else {
            document.getElementById('vendor_data_non_vendor').style.display = 'block';
        }


        if (get_user_setting_info("vendor_signed_up") == "1") {
            document.getElementById('vendor_signup_left_header').innerHTML = "Already signed up.";
            document.getElementById('vendor_signup_left_description').innerHTML = 'Awaiting approval';
            document.getElementById('vendor_signup_right_panel').style.display = 'none';
        }
    }
    else
        document.getElementById('vendor_data_non_vendor').style.display = 'block';
}

function vendor_add_offering() {
    var id_book = zen_auth_cjs.book_list[0].id_book;
    var id_doctype = "1";

    var doc_name = prompt("Title of new offering", "");
    if (doc_name == null)
        return;
    if (doc_name.length < 3) {
        alert('Name is too short');
        return;
    }

    var data_init = JSON.stringify(global_default_document);
    var doc_add_info = zen_auth_cjs.get_default_document(id_book, doc_name, id_doctype, data_init);

    zen_auth_cjs.create_document(doc_add_info).then(function (data) {
        if (data['id_result'] < 0) {
            zen_auth_cjs.show_error_message(data['description']);
        }
        else {
            var id_document = data['id_result'];
            vendor_fetch_offerings(id_document);
        }
    });


}
function vendor_fetch_offerings(id_doc_to_select) {
    var packet = zen_auth_cjs.list_document_default_packet();
    if (document.getElementById('vendor_offerings_active_filter').checked)
        packet['status'] = '1';
    
    zen_auth_cjs.list_documents(packet).then(function (data, status) {
        vendor_fill_offering_table(data, id_doc_to_select);
    });
}
function vendor_fill_offering_table(data, id_doc_to_select) {
    var table = document.getElementById('vendor_offering_list_table');
    table.innerHTML = '';

    var new_row = table.insertRow(-1);
    new_row.innerHTML = '<th>Offering</th><th>Active</th><th>Start</th><th>End</th><th>Categories</th><th>Destination</th><th>Range</th><th></th><th></th>';

    global_vendor_last_offerings = {};
    for (var c in data.doc_list) {
        var new_row = table.insertRow(-1);
        var inner_html = '';
        var d_row = data.doc_list[c];

        inner_html += '<td>' + d_row.document_name + '</td>';
        inner_html += '<td>' + (d_row.status == 1 ? 'Yes' : 'No') + '</td>';
        inner_html += '<td>' + local_format_date(d_row.date_start) + '</td>';
        inner_html += '<td>' + local_format_date(d_row.date_end) + '</td>';
        inner_html += '<td>' + d_row.category_1 + ' / ' + d_row.category_2 + ' / ' + d_row.category_3 + '</td>';
        inner_html += '<td>' + d_row.city + '</td>';
        inner_html += '<td>' + d_row.min_stay.toString() + '-' + d_row.max_stay.toString() + '</td>';
        inner_html += '<td><a href="/Offering.html">Edit</a></td>';
        inner_html += '<td><button class="btn btn-success" onclick="vendor_edit_offering_document(' + d_row.id_document + '); return false;">Old Edit</button></td>';
        inner_html += '<td><a href="/ManageListing.html">open</a></td>';

        new_row.innerHTML = inner_html;
        global_vendor_last_offerings[d_row.id_document] = d_row;
    }
}
function vendor_edit_offering_document(id_document) {
    global_vendor_current_document_id = id_document;
    document.getElementById('vendor_edit_offering_name').value = global_vendor_last_offerings[id_document].document_name;
    document.getElementById('vendor_edit_offering_city').value = global_vendor_last_offerings[id_document].city;
    document.getElementById('vendor_edit_offering_promotional_image').value = global_vendor_last_offerings[id_document].promotional_image;
    document.getElementById('vendor_edit_offering_offering_image').value = global_vendor_last_offerings[id_document].offering_image;
    document.getElementById('vendor_edit_offering_category_1').value = global_vendor_last_offerings[id_document].category_1;
    document.getElementById('vendor_edit_offering_category_2').value = global_vendor_last_offerings[id_document].category_2;
    document.getElementById('vendor_edit_offering_category_3').value = global_vendor_last_offerings[id_document].category_3;
    document.getElementById('vendor_edit_offering_min_stay').value = global_vendor_last_offerings[id_document].min_stay;
    document.getElementById('vendor_edit_offering_max_stay').value = global_vendor_last_offerings[id_document].max_stay;
    document.getElementById('vendor_edit_offering_id').value = id_document.toString();
    document.getElementById('vendor_edit_offering_id_display').value = id_document.toString();

    document.getElementById('vendor_edit_offering_available_date').value = local_format_date(global_vendor_last_offerings[id_document].date_start);
    document.getElementById('vendor_edit_offering_end_date').value = local_format_date(global_vendor_last_offerings[id_document].date_end);

    document.getElementById('vendor_edit_offering_status').checked = (global_vendor_last_offerings[id_document].status == 1);

    var doc_data = JSON.parse(global_vendor_last_offerings[id_document].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    for (var c = 0, l = content_field_list.length; c < l; c++)
    {
        var key_name = content_field_list[c];
        var data = doc_data.properties['offering_' + key_name];
        if (data == undefined)
            data = '';
        document.getElementById('vendor_edit_offering_' + key_name).value = data;
    }

    vendor_whatwehave_fill_tab_list(doc_data);
    vendor_whatwezen_fill_tab_list(doc_data);

    vendor_offering_amenity_fill_tab(global_vendor_last_offerings[id_document].amenity_list);
    $('#vendor_edit_offering_view').modal('show');
}

function vendor_whatwehave_fill_tab_list(doc_data) {
    var list = document.getElementById('vendor_offering_whatwehave_list');
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');


    list.innerHTML = '';

    var list_html = '';
    for (var c = 0, l = array.length; c < l; c++) {
        var new_item = '<a href="#" index="' + c.toString() + '" class="list-group-item">' + array[c] + '</a>';
        list_html += new_item;
    }
    list.innerHTML = list_html;

    $('#vendor_offering_whatwehave_list a').on('click', function () {
        $('#vendor_offering_whatwehave_textbox').val($(this).text());
        $('#vendor_offering_whatwehave_index').val($(this).attr('index'));
    });
}
function vendor_offering_whatwehave_saveitem() {
    var index = $('#vendor_offering_whatwehave_index').val();

    var item_text = $('#vendor_offering_whatwehave_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array[index] = item_text;
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
}
function vendor_offering_whatwehave_removeitem() {
    var index = $('#vendor_offering_whatwehave_index').val();

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwehavelist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array.splice(index, 1);
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
    $('#vendor_offering_whatwehave_index').val(-1);
}
function vendor_offering_whatwehave_upitem() {
    var index = Number($('#vendor_offering_whatwehave_index').val());

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwehavelist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 1 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index - 1];
    array[Number(index) - 1] = temp;
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
    $('#vendor_offering_whatwehave_index').val(-1);

}
function vendor_offering_whatwehave_downitem() {
    var index = Number($('#vendor_offering_whatwehave_index').val());

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwehavelist');

    if (index < 0 || index >= array.length - 1) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index + 1];
    array[Number(index) + 1] = temp;
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
    $('#vendor_offering_whatwehave_index').val(-1);

}
function vendor_offering_whatwehave_additem() {
    var item_text = $('#vendor_offering_whatwehave_textbox').val();

    var item_text = $('#vendor_offering_whatwehave_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwehavelist'];
    if (array == undefined) {
        array = [];
    }

    array.push(item_text);
    doc_data.properties['whatwehavelist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwehave_fill_tab_list(doc_data);
}

function vendor_whatwezen_fill_tab_list(doc_data) {
    var list = document.getElementById('vendor_offering_whatwezen_list');
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');


    list.innerHTML = '';

    var list_html = '';
    for (var c = 0, l = array.length; c < l; c++) {
        var new_item = '<a href="#" index="' + c.toString() + '" class="list-group-item">' + array[c] + '</a>';
        list_html += new_item;
    }
    list.innerHTML = list_html;

    $('#vendor_offering_whatwezen_list a').on('click', function () {
        $('#vendor_offering_whatwezen_textbox').val($(this).text());
        $('#vendor_offering_whatwezen_index').val($(this).attr('index'));
    });
}
function vendor_offering_whatwezen_saveitem() {
    var index = $('#vendor_offering_whatwezen_index').val();

    var item_text = $('#vendor_offering_whatwezen_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array[index] = item_text;
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
}
function vendor_offering_whatwezen_removeitem() {
    var index = $('#vendor_offering_whatwezen_index').val();

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwezenlist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 0 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    array.splice(index, 1);
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
    $('#vendor_offering_whatwezen_index').val(-1);
}
function vendor_offering_whatwezen_upitem() {
    var index = Number($('#vendor_offering_whatwezen_index').val());

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwezenlist'];
    if (array == undefined) {
        array = [];
    }

    if (index < 1 || index >= array.length) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index - 1];
    array[Number(index) - 1] = temp;
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
    $('#vendor_offering_whatwezen_index').val(-1);

}
function vendor_offering_whatwezen_downitem() {
    var index = Number($('#vendor_offering_whatwezen_index').val());

    var doc_data = zen_process_doc_data(global_vendor_last_offerings[global_vendor_current_document_id]);
    var array = zen_get_doc_array(doc_data, 'whatwezenlist');

    if (index < 0 || index >= array.length - 1) {
        alert('index out of bounds');
        return;
    }

    var temp = array[index];
    array[index] = array[index + 1];
    array[Number(index) + 1] = temp;
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
    $('#vendor_offering_whatwezen_index').val(-1);

}
function vendor_offering_whatwezen_additem() {
    var item_text = $('#vendor_offering_whatwezen_textbox').val();

    var item_text = $('#vendor_offering_whatwezen_textbox').val();
    if (item_text.length < 1) {
        alert('Item must have some text');
        return;
    }

    var doc_data = JSON.parse(global_vendor_last_offerings[global_vendor_current_document_id].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    var array = doc_data.properties['whatwezenlist'];
    if (array == undefined) {
        array = [];
    }

    array.push(item_text);
    doc_data.properties['whatwezenlist'] = array;
    global_vendor_last_offerings[global_vendor_current_document_id].content = JSON.stringify({document: doc_data});
    vendor_whatwezen_fill_tab_list(doc_data);
}

function vendor_offering_amenity_fill_tab(list_data) {
    var amenity_list = gauth_browser_info.amenity_list;
    if (list_data == undefined)
        list_data = '';
    var selected_list = list_data.split(',');

    for (var c = 0, l = amenity_list.length; c < l; c += 2) {
        var id = 'vendor_edit_offering_amenity_item_' + c.toString();
        if ($.inArray(c.toString(), selected_list) != -1)
            document.getElementById(id).checked = true;
        else
            document.getElementById(id).checked = false;
    }
}


function vendor_offering_save_details() {
    var id_document = Number(document.getElementById('vendor_edit_offering_id').value);
    var document_name = document.getElementById('vendor_edit_offering_name').value;
    var is_active = document.getElementById('vendor_edit_offering_status').checked;
    var date_start = document.getElementById('vendor_edit_offering_available_date').value;
    var date_end = document.getElementById('vendor_edit_offering_end_date').value;
    var city = document.getElementById('vendor_edit_offering_city').value;
    var promotional_image = document.getElementById('vendor_edit_offering_promotional_image').value;
    var offering_image = document.getElementById('vendor_edit_offering_offering_image').value;
    var category_1 = document.getElementById('vendor_edit_offering_category_1').value;
    var category_2 = document.getElementById('vendor_edit_offering_category_2').value;
    var category_3 = document.getElementById('vendor_edit_offering_category_3').value;
    var min_stay = Number(document.getElementById('vendor_edit_offering_min_stay').value);
    var max_stay = Number(document.getElementById('vendor_edit_offering_max_stay').value);

    if (isNaN(min_stay))
        min_stay = 0;
    if (isNaN(max_stay))
        max_stay = 0;
    //     url: "/document/update",
    var doc_data = JSON.parse(global_vendor_last_offerings[id_document].content).document;
    if (doc_data == undefined)
        doc_data = global_default_document.document;

    for (var c = 0, l = content_field_list.length; c < l; c++)
        doc_data.properties['offering_' + content_field_list[c]] = document.getElementById('vendor_edit_offering_' + content_field_list[c]).value;

    var amenity_choices = vendor_offering_scrape_amenity_selections();

    //get change list
    var packet = {
        browser_token: zen_auth_cjs.browser_info["browser_token"],
        id_document: id_document,
        document_name: document_name,
        city: city,
        date_start: date_start,
        date_end: date_end,
        search_tags: '',
        status: is_active ? 1 : 0,
        promotional_image: promotional_image,
        offering_image: offering_image,
        amenity_list: amenity_choices,
        category_1: category_1,
        category_2: category_2,
        category_3: category_3,
        min_stay: min_stay,
        max_stay: max_stay,
        content: JSON.stringify({document: doc_data})
    };
    zen_auth_cjs.save_document(packet).then(function (data) {
        $('#vendor_edit_offering_view').modal('hide');
        vendor_fetch_offerings(id_document);
    });
}
$(document).ready(function () {
    $('.datepicker').datepicker({
        format: "mm/dd/yyyy"
    });

});

function upload_file_for_client_to_S3(field_to_output_tag, callback, id_document) {
    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    var file_name = file_domNode.value.replace(/^.*[\\\/]/, '');
    if (file_name == "")
        return;

    var upload_content = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book,
        file_path: file_name,
        public: 1
    };
    gauth_show_busy_icon(1);
    $(field_to_output_tag).val('Uploading...');
    $.post("/book/upload_policy", upload_content,
            function (data, status) {
                // var s_form = document.getElementById('s3_file_submit_form');
                $('#s3_file_submit_form').attr('action', gauth_browser_info.profile_doc.bucket_url);
                $('#s3_file_submit_form input[name="key"]').attr('value', data['target_file_name']);
                $('#s3_file_submit_form input[name="signature"]').attr('value', data['signature']);
                $('#s3_file_submit_form input[name="policy"]').attr('value', data['policy_base64']);
                $('#s3_file_submit_form input[name="content-type"]').attr('value', data['content_type']);
                $('#s3_file_submit_form input[name="AWSAccessKeyId"]').attr('value', data['AWSAccessKeyId']);
                $('#s3_file_submit_form input[name="acl"]').attr('value', data['acl']);

                var form_dom = $('#s3_file_submit_form')[0];
                var vFD = new FormData(form_dom);
                var oXHR = new XMLHttpRequest();
                var full_path = gauth_browser_info.profile_doc.bucket_url + data['target_file_name'];
                oXHR.addEventListener('load', function () {
                    gauth_show_busy_icon(-1);
                    $(field_to_output_tag).val(full_path);
                    callback();
                }, false);
                oXHR.addEventListener('error', function (a, b, c) {
                    alert('error');
                }, false);
                oXHR.open('POST', gauth_browser_info.profile_doc.bucket_url, true);
                oXHR.send(vFD);
                gauth_show_busy_icon(-1);
            });
}
function vendor_offering_upload(text_box) {
    var id_document = Number(document.getElementById('vendor_edit_offering_id').value);
    var result_f = document.getElementById(text_box);
    result_f.value = '';

    var file_domNode = $('#vendor_edit_offering_upload_image')[0];
    file_domNode.value = '';
    file_domNode.onchange = function (evt) {
        upload_file_for_client_to_S3('#' + text_box, function () {

        }, id_document);
    };
    file_domNode.click();
}
function vendor_fetch_file_list() {
    var packet = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book
    };
    gauth_show_busy_icon(1);
    $.post("/book/file_list", packet, function (data, status) {
        var table = document.getElementById('vendor_offering_files_table');
        table.innerHTML = '';

        for (var c in data.dir_result) {
            var new_row = table.insertRow(-1);
            var inner_html = '';
            var d_row = data.dir_result[c];

            var file_name = d_row.Key.split('/')[3];

            inner_html += '<td>' + file_name + ' &nbsp; &nbsp; <button onclick="vendor_delete_file(\'' + file_name + '\')">Delete</button>';
            inner_html += '<br><img class="image_preview" src="' + gauth_browser_info.profile_doc.bucket_url + d_row.Key + '" />';
            inner_html += '</td>';

            new_row.innerHTML = inner_html;
        }
        gauth_show_busy_icon(-1);
    });

}
function vendor_delete_file(file_key) {

    var packet = {
        browser_token: gauth_browser_info["browser_token"],
        id_book: gauth_book_list[0].id_book,
        file_path: file_key,
        public: 1
    };
    gauth_show_busy_icon(1);
    $.post("/book/delete_file", packet, function (data, status) {
        vendor_fetch_file_list();
        gauth_show_busy_icon(-1);
    });



}

function vendor_init_category_lists() {
    var category_list = gauth_browser_info.category_list;

    var option_html = '';
    for (var c = 0, l = category_list.length; c < l; c++)
        option_html += '<option>' + category_list[c] + '</option>';

    document.getElementById('vendor_edit_offering_category_1').innerHTML = option_html;
    document.getElementById('vendor_edit_offering_category_2').innerHTML = option_html;
    document.getElementById('vendor_edit_offering_category_3').innerHTML = option_html;
}
function vendor_init_amenities_tab() {
    var am_tab = document.getElementById('vendor_offering_amenities_tab');
    am_tab.innerHTML = '';

    var amenity_list = gauth_browser_info.amenity_list;

    var amenity_html = '';
    for (var c = 0, l = amenity_list.length; c < l; c += 2)
        amenity_html += '<div class="amenity_item"><input type="checkbox" id="vendor_edit_offering_amenity_item_' + c.toString() + '" />'
                + '<label for="vendor_edit_offering_amenity_item_' + c.toString() + '">' + amenity_list[c] + ' (' + amenity_list[c + 1] + ')</label></div>';
    am_tab.innerHTML = amenity_html;
}
function vendor_offering_scrape_amenity_selections() {
    var amenity_list = gauth_browser_info.amenity_list;

    var selected_list = '';
    for (var c = 0, l = amenity_list.length; c < l; c += 2) {
        var id = 'vendor_edit_offering_amenity_item_' + c.toString();
        if (document.getElementById(id).checked)
            selected_list += c.toString() + ',';
    }

    if (selected_list != '')
        selected_list = selected_list.substring(0, selected_list.length - 1);
    return selected_list;
}

function vendor_initialize_signup_data() {
    zen_auth_cjs.set_user_setting_info('vendor_signed_up', "1");
    zen_auth_cjs.set_user_setting_info('vendor_signup_hotel_name', document.getElementById('vendor_signup_hotel_name').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_street_address', document.getElementById('vendor_signup_street_address').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_city', document.getElementById('vendor_signup_city').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_state', document.getElementById('vendor_signup_state').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_zipcode', document.getElementById('vendor_signup_zipcode').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_country', document.getElementById('vendor_signup_country').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_website', document.getElementById('vendor_signup_website').value);

    zen_auth_cjs.set_user_setting_info('vendor_signup_first_name', document.getElementById('vendor_signup_first_name').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_last_name', document.getElementById('vendor_signup_last_name').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_hotel_role', document.getElementById('vendor_signup_hotel_role').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_email', document.getElementById('vendor_signup_email').value);
    zen_auth_cjs.set_user_setting_info('vendor_signup_phone', document.getElementById('vendor_signup_phone').value);
    window.location = location.protocol + '//' + location.host + location.pathname;
}

function gauth_register_new_vendor() {
    //if they are logged in, use the existing account
    if (gauth_logged_in_user_name != '') {
        vendor_initialize_signup_data();
    }
    else {
        document.getElementById('gauth_logon_register_email').value = document.getElementById('vendor_signup_email').value;
        document.getElementById('gauth_logon_register_displayname').value = document.getElementById('vendor_signup_first_name').value;

        gauth_vendor_user_profile = {
            vendor_signed_up: "1",
            vendor_signup_hotel_name: document.getElementById('vendor_signup_hotel_name').value,
            vendor_signup_street_address: document.getElementById('vendor_signup_street_address').value,
            vendor_signup_city: document.getElementById('vendor_signup_city').value,
            vendor_signup_state: document.getElementById('vendor_signup_state').value,
            vendor_signup_zipcode: document.getElementById('vendor_signup_zipcode').value,
            vendor_signup_country: document.getElementById('vendor_signup_country').value,
            vendor_signup_website: document.getElementById('vendor_signup_website').value,
            vendor_signup_first_name: document.getElementById('vendor_signup_first_name').value,
            vendor_signup_last_name: document.getElementById('vendor_signup_last_name').value,
            vendor_signup_hotel_role: document.getElementById('vendor_signup_hotel_role').value,
            vendor_signup_email: document.getElementById('vendor_signup_email').value,
            vendor_signup_phone: document.getElementById('vendor_signup_phone').value
        };

        $('#admin_view_add_client').modal('show');
    }

    return false;
}
