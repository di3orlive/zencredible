define({
    authengine: function () {
        function eng() {
            this.booklist = [];
            this.book_member_list = [];
            this.browser_info = {};
            this.logged_in_user_name = ""; // != "" if logged in
            this.ajax_busy_counter = 0;
            this.ajax_busy_counter_query_regex = '.dashboard_dynamic_network_status_icon';
        }

//create tokens
//this tries to use stored cookies, otherwise relogs as a guest if fail
// call authorise_user()
        eng.prototype.init_browser_token = function () {
            var dfd = $.Deferred();
            var username_cookie = this.getCookie('gauth_login_cookie_username');
            var userpasshash_cookie = this.getCookie('gauth_login_cookie_password_hash');
            var login_info = {access_type: 0, email: '', password: ''};
            if (username_cookie != undefined && username_cookie != null
                    && username_cookie != '') {
                login_info.access_type = 1;
                login_info.email = username_cookie;
                login_info.password = userpasshash_cookie;
            }
            var me = this;
            this.authorize_user(login_info).then(function (data) {
                dfd.resolve(data);
            },
                    function (reject) {
                        login_info.email = '';
                        login_info.password = '';
                        me.authorize_user(login_info).then(function (data) {
                            dfd.resolve(data);
                        });
                    });
            return dfd.promise();
        };
        //this authorizes only from passed in creditionals
        eng.prototype.authorize_user = function (user_data) {
            var dfd = $.Deferred();
            var password = user_data.password;
            var email = user_data.email;
            var login_info = {access_type: 1, email: email, password: password};
            this.clear_auth_cookies();
            this.show_busy_icon(1);
            var me = this;
            $.post("/auth/browser_token", login_info, function (data) {
                if (data.profile_doc.id_user <= 0 && data.profile_doc.display_name != 'Guest') {
                    me.clear_auth_cookies();
                    me.show_busy_icon(-1);
                    dfd.reject("Login failed");
                    return;
                }

                //always store username/password
                me.setCookie('gauth_login_cookie_username', email, 1000);
                me.setCookie('gauth_login_cookie_password_hash', password, 14);
                me.browser_info = data;
                me.book_list = data.book_list;
                me.book_member_list = data.book_member_list;
                me.logged_in_user_name = data.profile_doc.email;
                me.show_busy_icon(-1);
                dfd.resolve(data);
            });
            return dfd.promise();
        };
        //clears cookies
        eng.prototype.logout = function () {
            this.clear_auth_cookies();
            location.reload(true);
        };
        //these get and set data on a per user level
        eng.prototype.get_user_setting_info = function (setting_name, default_value) {
            if (default_value == undefined)
                default_value = '';
            if (this.logged_in_user_name == '')
                return default_value;
            var user_profile = this.user_profile_get();
            if (user_profile[setting_name] != undefined)
                return user_profile[setting_name];
            return default_value;
        };
        eng.prototype.set_user_setting_info = function (setting_name, new_value) {
            if (this.logged_in_user_name == '')
                return;
            //new way
            var user_profile = this.user_profile_get();
            if (user_profile[setting_name] != new_value) {
                user_profile[setting_name] = new_value;
                var base_user_record_update_info = {browser_token: this.browser_info["browser_token"]};
                base_user_record_update_info['user_profile'] = JSON.stringify(user_profile);
                this.base_record_change(base_user_record_update_info);
                this.user_profile_set(user_profile);
                return true;
            }

            return false;
        };
        //get / set entire profile
        eng.prototype.user_profile_get = function () {
            var user_profile_str = this.browser_info.profile_doc.user_profile;
            if (!user_profile_str || user_profile_str == '')
                user_profile_str = '{}';
            try {
                return JSON.parse(user_profile_str);
            }
            catch (e) {
                return {};
            }
        };
        eng.prototype.user_profile_set = function (user_profile) {
            var user_profile_str = JSON.stringify(user_profile);
            this.browser_info.profile_doc.user_profile = user_profile_str;
        };
        eng.prototype.base_record_change = function (update_info, success_callback) {
            this.show_busy_icon(1);
            var me = this;
            $.post("/auth/user_update", update_info, function (data, status) {
                if (data.result.id_result < 0) {
                    me.show_msg_dialog("Save user data failed");
                }
                else if (success_callback != undefined)
                    success_callback();
                me.show_busy_icon(-1);
            });
        };
        eng.prototype.change_password = function () {
            var old_pw = prompt('Old Password');
            var new_pw = prompt('New Password');
            var verify = prompt('Verify New Password');
            var error = "";
            if (new_pw == null)
                new_pw = '';
            if (new_pw != verify)
                error = "password_change_verify_not_match";
            if (new_pw.length < 3)
                error = "password_change_too_short";
            if (error != "") {
                this.show_msg_dialog(error);
                return;
            }

            var base_user_record_update_info = {browser_token: this.browser_info["browser_token"]};
            if (window.b64_sha256 == undefined) {
                gauth_show_busy_icon(1);
                var x = document.createElement('script');
                x.src = '/scripts/sha256.js';
                x.onload = function () {
                    base_user_record_update_info['password_hash'] = b64_sha256(new_pw);
                    base_user_record_update_info['old_password_hash'] = b64_sha256(old_pw);
                    commit_user_base_record_change(base_user_record_update_info, function () {
                        zen_auth_cjs.show_msg_dialog('password changed');
                    });
                    gauth_show_busy_icon(-1);
                };
                document.getElementsByTagName("head")[0].appendChild(x);
            }
            else {
                base_user_record_update_info['password_hash'] = b64_sha256(new_pw);
                base_user_record_update_info['old_password_hash'] = b64_sha256(old_pw);
                commit_user_base_record_change(base_user_record_update_info, function () {
                    zen_auth_cjs.show_msg_dialog('password changed');
                });
            }
        };
        //get/set/create/list data on a document level
        eng.prototype.save_document = function (packet) {
            var dfd = $.Deferred();
            this.show_busy_icon(1);
            var me = this;
            $.post("/document/update", packet, function (data, status) {
                dfd.resolve(data);
                me.show_busy_icon(-1);
            });
            return dfd.promise();
        };
        eng.prototype.create_document = function (packet) {
            var dfd = $.Deferred();
            this.show_busy_icon(1);
            var me = this;
            $.post("/document/create", packet, function (data, status) {
                dfd.resolve(data);
                me.show_busy_icon(-1);
            });
            return dfd.promise();
        };
        eng.prototype.get_default_document = function (id_book, doc_name, id_doctype, data_init) {
            return {
                browser_token: this.browser_info["browser_token"],
                id_book: id_book,
                document_name: doc_name,
                public_basepath: '',
                private_basepath: '',
                id_doctype: id_doctype,
                enabled: 1,
                tags: '',
                status: 0,
                data_init: data_init
            };
        };
        eng.prototype.list_documents = function (packet) {
            var dfd = $.Deferred();
            this.show_busy_icon(1);
            var me = this;
            $.post("/document/list", packet, function (data, status) {
                dfd.resolve(data);
                me.show_busy_icon(-1);
            });
            return dfd.promise();
        };
        eng.prototype.list_document_default_packet = function () {
            var packet =
                    {max_records: "50",
                        book_list: '',
                        sort_column: 'doc_edit',
                        sort_order: 'desc'
                    };
            packet['doctype'] = "1";
            packet['browser_token'] = this.browser_info["browser_token"];
            return packet;
        };
        //utility functions
        eng.prototype.getCookie = function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1);
                if (c.indexOf(name) == 0)
                    return c.substring(name.length, c.length);
            }
            return "";
        };
        eng.prototype.setCookie = function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        };
        eng.prototype.getParameterByName = function (name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        };
        eng.prototype.clear_auth_cookies = function () {
            document.cookie = 'gauth_login_cookie_username=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = 'gauth_login_cookie_password_hash=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = 'gauth_login_cookie_client_code=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        };
        eng.prototype.show_msg_dialog = function (text) {
            alert(text);
        };
        eng.prototype.show_busy_icon = function (cnt) {
            this.ajax_busy_counter += cnt;
            var icon_status_query = $(this.ajax_busy_counter_query_regex);
            if (this.ajax_busy_counter > 0) {
                icon_status_query.removeClass('network_green');
                icon_status_query.addClass('network_yellow');
            }
            else {
                icon_status_query.addClass('network_green');
                icon_status_query.removeClass('network_yellow');
            }
        };
        //routine to create a book for vendor type user
        eng.prototype.vendor_verify_create_singleton_book = function (data) {
            var book_add_info = {
                browser_token: data["browser_token"],
                book_name: 'VendorBook',
                public_basepath: '',
                private_basepath: '',
                enabled: 1,
                tags: ''
            };
            zen_auth_cjs.show_busy_icon(1);
            $.post("/book/create", book_add_info, function (data, status) {
                if (data['id_book'] <= 0) {
                    alert('failed to create vendor book - contact support');
                    return;
                }

                //login again so book list is fetched
                zen_auth_cjs.init_browser_token();
                zen_auth_cjs.show_busy_icon(-1);
            });
        };
        return new eng();
    }
});



    