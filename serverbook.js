var exports = module.exports = {};

var http = require('http');
var parseString = require('xml2js').parseString;
var sql = require('mssql');
var AWS = require('aws-sdk');
exports['init_cache'] = function(app) {
    app.logpost('/book/all_fetch', function(request, response, next) {
        exports.all_fetch(request, response, next);
    });
    app.logpost('/book/admin_fetch', function(request, response, next) {
        exports.admin_fetch(request, response, next);
    });
    app.logpost('/book/update', function(request, response, next) {
        exports.update(request, response, next);
    });
    app.logpost('/book/create', function(request, response, next) {
        exports.create(request, response, next);
    });
    app.logpost('/book/delete', function(request, response, next) {
        exports.delete(request, response, next);
    });
    app.logpost('/book/remove_member', function(request, response, next) {
        exports.remove_member(request, response, next);
    });
    app.logpost('/book/add_member', function(request, response, next) {
        exports.add_member(request, response, next);
    });
    app.logpost('/book/suggest', function(request, response, next) {
        exports.suggest(request, response, next);
    });
    app.logpost('/book/add_access_code', function(request, response, next) {
        exports.add_access_code(request, response, next);
    });
    app.logpost('/book/update_access_code', function(request, response, next) {
        exports.update_access_code(request, response, next);
    });
    app.logpost('/book/submit_access_code', function(request, response, next) {
        exports.submit_access_code(request, response, next);
    });
    app.logpost('/book/delete_access_code', function(request, response, next) {
        exports.delete_access_code(request, response, next);
    });
    app.logpost('/book/file_list', function(request, response, next) {
        exports.file_list(request, response, next);
    });
    app.logpost('/book/upload_policy', function(request, response, next) {
        exports.upload_policy(request, response, next);
    });
    app.logpost('/book/delete_file', function(request, response, next) {
        exports.delete_file(request, response, next);
    });
    app.logpost('/book/clone_file', function(request, response, next) {
        exports.clone_file(request, response, next);
    });
};
exports['all_fetch'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var query = 'select * from [auth].[books_4_usertoken] where user_token = @token order by book_name;' +
                'select * from [auth].[book_members_4_usertoken]  where user_token = @token';
        var sqlRequest = new sql.Request(connection);
        sqlRequest.multiple = true;
        sqlRequest.input("token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.query(query,
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('book/all_fetch failed query');
                        console.log(err.message);
                        return;
                    }
                    var json_response = {book_list: recordsets[0], book_member_list: recordsets[1]};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
exports['admin_fetch'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }
        var query = 'select * from [auth].[books_4_usertoken] where user_token = @token  order by book_name; ' +
                'select * from [auth].[book_members_4_admintoken]  where admin_token = @token; ' +
                'select * from auth.access_codes_4_admintoken  where admin_token = @token;';
        var sqlRequest = new sql.Request(connection);
        sqlRequest.multiple = true;
        sqlRequest.input("token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.query(query,
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('book/all_fetch failed query');
                        console.log(err.message);
                        return;
                    }
                    var json_response = {book_list: recordsets[0], book_user_list: recordsets[1], access_code_list: recordsets[2]};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
exports['update'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_book', sql.NVarChar(1000), request.body['id_book']);
        sqlRequest.input('book_name', sql.NVarChar(1000), request.body['book_name']);
        sqlRequest.input('enabled', sql.NVarChar(1000), request.body['enabled']);
        sqlRequest.input('tags', sql.NVarChar(1000), request.body['tags']);
        sqlRequest.execute('auth.book_update',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify({result: recordsets[0]}));
                });
    });
};
exports['create'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('book_name', sql.NVarChar(1000), request.body['book_name']);
        sqlRequest.input('private_basepath', sql.NVarChar(1000), request.body['private_basepath']);
        sqlRequest.input('public_basepath', sql.NVarChar(1000), request.body['public_basepath']);
        sqlRequest.input('enabled', sql.NVarChar(1000), request.body['enabled']);
        sqlRequest.input('tags', sql.NVarChar(1000), request.body['tags']);
        sqlRequest.execute('auth.book_create',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(recordsets[0][0]));
                });
    });
};
exports['delete'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_book', sql.NVarChar(1000), request.body['id_book']);
        sqlRequest.execute('auth.book_delete',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    var policy_tbl = recordsets[0];
                    var id_result = policy_tbl[0]['id_result'];
                    if (id_result > 0) {
                        var bucket_name = policy_tbl[0]['bucket_name'];
                        var base_path = policy_tbl[0]['book_publicpath'];
                        //tbl_policy.Rows[0]["book_privatepath"].ToString() //private not used at this time

                        var base_params = {
                            Bucket: bucket_name,
                            Prefix: base_path
                        };
                        var s3 = new AWS.S3({accessKeyId: policy_tbl[0]['public_key'],
                            secretAccessKey: policy_tbl[0]['private_key'],
                            region: 'us-east-1'});
                        var object_list = [];
                        S3_get_full_object_list(s3, true, base_params, object_list, null, function(r_base_params, r_object_list) {
                            var json_response = {
                                id_result: "1"
                            };
                            response.writeHead(200, {"Content-Type": "application/json"});
                            response.end(JSON.stringify(json_response));
                        });
                    }
                    else
                    {
                        response.writeHead(200, {"Content-Type": "application/json"});
                        response.end(JSON.stringify(policy_tbl[0]));
                    }
                });
    });
};
exports['add_access_code'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_book', sql.NVarChar(1000), request.body.id_book);
        sqlRequest.input('code', sql.NVarChar(1000), request.body.code);
        sqlRequest.input('role_name', sql.NVarChar(1000), request.body.role_name);
        sqlRequest.input('date_start', sql.NVarChar(1000), request.body.date_start);
        sqlRequest.input('date_end', sql.NVarChar(1000), request.body.date_end);
        sqlRequest.input('count', sql.NVarChar(1000), request.body.count);
        sqlRequest.execute('auth.book_add_access_code',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(recordsets[0]));
                });
    });
};
exports['update_access_code'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }
        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_code', sql.NVarChar(1000), request.body['id_code']);
        sqlRequest.input('code', sql.NVarChar(1000), request.body['code']);
        sqlRequest.input('date_start', sql.NVarChar(1000), request.body['date_start']);
        sqlRequest.input('date_end', sql.NVarChar(1000), request.body['date_end']);
        sqlRequest.input('id_role', sql.NVarChar(1000), request.body['id_role']);
        sqlRequest.input('ctr_usage', sql.NVarChar(1000), request.body['ctr_usage']);
        sqlRequest.execute('auth.book_update_access_code',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify({result: recordsets[0]}));
                });
    });
};
exports['submit_access_code'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('code', sql.NVarChar(1000), request.body['code']);
        sqlRequest.input('opt', sql.NVarChar(1000), request.body['opt']);
        sqlRequest.execute('auth.book_submit_access_code',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(recordsets[0][0]));
                });
    });
};
exports['delete_access_code'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_code', sql.NVarChar(1000), request.body['id_code']);
        sqlRequest.execute('auth.book_delete_access_code',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify({result: recordsets[0]}));
                });
    });
};
exports['remove_member'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_book', sql.NVarChar(1000), request.body['id_book']);
        sqlRequest.input('id_user', sql.NVarChar(1000), request.body['id_user']);
        sqlRequest.execute('auth.book_remove_member',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(recordsets[0][0]));
                });
    });
};
exports['add_member'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('id_book', sql.NVarChar(1000), request.body['id_book']);
        sqlRequest.input('member_email', sql.NVarChar(1000), request.body['member_email']);
        sqlRequest.input('id_role', sql.NVarChar(1000), request.body['id_role']);
        sqlRequest.execute('auth.book_add_member',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify({result: recordsets[0][0]}));
                });
    });
};
exports['suggest'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('key', sql.NVarChar(1000), "%" + request.body.key + "%");
        sqlRequest.input('pkey', sql.NVarChar(1000), request.body.key);
        sqlRequest.input('id_book', sql.NVarChar(1000), request.body['id_book']);
        sqlRequest.input('limit', sql.INT, parseInt(request.body.limit));
        var query_str = "select top (@limit) display_name, email, id_suggest_user, "
                + "  case when charindex(@pkey, email) - 1 = 0 then 0 else 1 end as directhit, "
                + " charindex(@pkey, email) as position  from auth.book_suggest_4_admintoken "
                + "where admin_token = @token and id_book = @id_book and ((display_name like @key) or (email like @key))"
                + " order by directhit, email";
        sqlRequest.query(query_str,
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(recordsets));
                });
    });
};
exports['file_list'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_book", sql.INT, parseInt(request.body.id_book));
        sqlRequest.execute('auth.book_file_list_policy',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('book/file_list failed query');
                        console.log(err.message);
                        return;
                    }

                    var row = recordsets[0][0];
                    var id_result = row["id"].toString();
                    if (parseInt(id_result) > 0) {
                        var private_key = row["private_key"].toString();
                        var public_key = row["public_key"].toString();
                        var public_basepath = row["public_basepath"].toString() + "public/";
                        var bucket_name = row["bucket_name"].toString();
                        var bucket_url = row["bucket_url"].toString();
                        var aws_url = "s3.amazonaws.com";
                        var expires = parseInt(((new Date()).getTime() / 1000) + 600).toString(); //600 seconds
                        var baseA = "GET\n\n\n" + expires + "\n/" + bucket_name + "/";
                        var signature = S3_generate_signature(baseA, private_key);
                        var aws_path = "/" + bucket_name + "/" + "?AWSAccessKeyId=" + public_key + "&Expires=" + expires + "&Signature="
                                + encodeURIComponent(signature)
                                + "&prefix=" + encodeURIComponent(public_basepath)
                                ;
                        var base_params = {
                            Bucket: bucket_name,
                            Prefix: public_basepath
                        };
                        var s3 = new AWS.S3({
                            accessKeyId: public_key,
                            secretAccessKey: private_key,
                            region: 'us-east-1'
                        });
                        var object_list = [];
                        S3_get_full_object_list(s3, false, base_params, object_list, null, function(r_base_params, r_object_list) {
                            var json_response = {
                                dir_result: object_list,
                                bucket_url: bucket_url,
                                public_basepath: public_basepath,
                                id_result: "1"
                            };
                            response.writeHead(200, {"Content-Type": "application/json"});
                            response.end(JSON.stringify(json_response));
                        });
                    }

                });
    });
};
exports['upload_policy'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_book", sql.INT, parseInt(request.body.id_book));
        sqlRequest.input("file_path", sql.NVarChar(4000), "public/" + request.body.file_path);
        sqlRequest.input("public", sql.NVarChar(4000), request.body.public);
        sqlRequest.execute('auth.book_upload_policy', function(err, recordsets, returnValue) {
            if (err != undefined) {
                console.log('book/upload_policy failed query');
                console.log(err.message);
                return;
            }

            var row = recordsets[0][0];
            var id_result = row["id_result"].toString();
            if (parseInt(id_result) > 0) {
                var private_key = row["private_key"].toString();
                var public_key = row["public_key"].toString();
                var policy_text = row["policy_text"].toString();
                var policy_base64 = Buffer(policy_text).toString('base64');
                var acl = row["acl"].toString();
                var target_file_name = row["out_file_name"].toString();

                var form_post = row["bucket_url"].toString();
                var signature = S3_generate_signature(policy_base64, private_key);

                var json_response = {policy_base64: policy_base64,
                    signature: signature,
                    target_file_name: target_file_name,
                    AWSAccessKeyId: public_key,
                    content_type: "",
                    acl: acl,
                    formpost: form_post};

                response.writeHead(200, {"Content-Type": "application/json"});
                response.end(JSON.stringify(json_response));
            }
            else
            {
                console.log('failed book/upload_policy');
            }
        });
    });
};
exports['delete_file'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_book", sql.INT, parseInt(request.body.id_book));
        sqlRequest.input("file_path", sql.NVarChar(4000), "public/" + request.body.file_path);
        sqlRequest.input("public", sql.NVarChar(1000), request.body.public);

        sqlRequest.execute('auth.book_delete_policy',
                function(err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log('book/delete_file failed query');
                        console.log(err.message);
                        return;
                    }

                    var row = recordsets[0][0];
                    var id_result = row["id_result"].toString();
                    if (parseInt(id_result) > 0) {
                        var private_key = row["private_key"].toString();
                        var public_key = row["public_key"].toString();
                        var bucket_name = row["bucket_name"].toString();
                        var bucket_url = row["bucket_url"].toString();
                        var form_post = bucket_url;
                        var target_file_name = row["out_file_name"].toString();
                        var aws_url = "s3.amazonaws.com";
                        var expires = parseInt(((new Date()).getTime() / 1000) + 600).toString(); //600 seconds
                        var baseA = "DELETE\n\n\n" + expires + "\n/" + bucket_name + "/" + target_file_name.replace(/ /g, "%20");
                        var signature = S3_generate_signature(baseA, private_key);
                        var aws_path = "/" + bucket_name + "/" + target_file_name.replace(/ /g, "%20")
                                + "?AWSAccessKeyId=" + public_key +
                                "&Expires=" + expires + "&Signature=" + encodeURIComponent(signature);

                        var http_request = http.request({host: aws_url, path: aws_path, method: 'DELETE'},
                        function(aws_response) {
                            var str = '';
                            //another chunk of data has been recieved, so append it to `str`
                            aws_response.on('data', function(chunk) {
                                str += chunk;
                            });
                            //the whole response has been recieved, so we just print it out here
                            aws_response.on('end', function() {
                                parseString(str, function(err, result) {
                                    if (err != undefined) {
                                        console.log('book/delete_file failed xml parse');
                                        console.log(err.message);
                                        return;
                                    }

                                    var json_response = {id_result: "1"};
                                    response.writeHead(200, {"Content-Type": "application/json"});
                                    response.end(JSON.stringify(json_response));
                                });

                            });
                        }).end();
                    }
                });
    });
};
exports['clone_file'] = function(request, response, next) {
    var connection = new sql.Connection(dbconfig, function(err)
    {
        if (err != undefined) {
            console.log(err.message, err);
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input("browser_token", sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input("id_book", sql.INT, parseInt(request.body.id_book));
        sqlRequest.input("file_path", sql.NVarChar(4000), "public/" + request.body.file_path);
        sqlRequest.input("public", sql.NVarChar(4), '1');
        sqlRequest.execute('auth.book_upload_policy', function(err, recordsets, returnValue) {
            if (err != undefined) {
                console.log('book/upload_policy failed query');
                console.log(err.message);
                return;
            }

            var row = recordsets[0][0];
            var id_result = row["id_result"].toString();
            if (parseInt(id_result) > 0) {
                var private_key = row["private_key"].toString();
                var public_key = row["public_key"].toString();
                var bucket_name = row["bucket_name"].toString();

                var params = {
                    Bucket: bucket_name,
                    CopySource: request.body.copy_source,
                    Key: request.body.full_path,
                    ACL: 'public-read'
                };
                var s3 = new AWS.S3({
                    accessKeyId: public_key,
                    secretAccessKey: private_key,
                    region: 'us-east-1'
                });
                s3.copyObject(params, function(err, data) {
                    if (err) {
                        console.log(err.message, err);
                        return;
                    }
                    // successful response
                    var json_response = {data: data};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
            }
            else
            {
                console.log('failed book/upload_policy');
            }
        });
    });
};
