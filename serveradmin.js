var exports = module.exports = {};
var sql = require('mssql');
exports['serverauth'] = { obj: null };

var app_cache = null;
var auth_cache = null;

exports['init_cache'] = function (app, auth) {
    app_cache = app;
    auth_cache = auth;
    app.logpost('/admin/user_list', function (request, response, next) {
        exports.user_list(request, response, next);
    });
    app.logpost('/admin/set_user_vendor', function (request, response, next) {
        exports.set_user_vendor(request, response, next);
    });
    app.logpost('/admin/fetch_settings', function (request, response, next) {
        exports.fetch_settings(request, response, next);
    });
    app.logpost('/admin/save_setting', function (request, response, next) {
        exports.save_setting(request, response, next);
    });
    app.logpost('/admin/post_s3_blob', function (request, response, next) {
        exports.post_s3_blob(request, response, next);
    })
};

exports['user_list'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var query = 'select top(@row_limit) * from auth.users where '
                + '(vendor_flag = @flag1 or vendor_flag = @flag2) '
                + ' and (email like @filter or display_name like @filter) ';

        var filter = '%' + request.body.text_filter + '%';
        var sqlRequest = new sql.Request(connection);
        var flag1 = 0;
        var flag2 = 1;
        if (request.body.vendor_filter == 1)
            flag2 = 0;
        if (request.body.vendor_filter == 2)
            flag1 = 1;

        sqlRequest.input('filter', sql.NVarChar(1000), filter);
        sqlRequest.input('flag1', sql.NVarChar(1000), flag1);
        sqlRequest.input('flag2', sql.NVarChar(1000), flag2);
        sqlRequest.input('row_limit', sql.INT, request.body.row_limit);

        // sqlRequest.input('access_type', sql.INT, parseInt(request.body.access_type));
        sqlRequest.query(query, function (err, recordset, returnValue) {
            if (err != undefined) {
                return;
            }

            var json_response = {
                status: 1,
                user_list: recordset
            };
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(json_response));
        });
    });
};

exports['set_user_vendor'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var query = 'update auth.users set vendor_flag=@vendor where id=@id';
        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('vendor', sql.NVarChar(1000), request.body.vendor);
        sqlRequest.input('id', sql.NVarChar(1000), request.body.id);
        sqlRequest.query(query, function (err, recordset, returnValue) {
            if (err != undefined) {
                return;
            }

            var json_response = {
                status: 1
            };
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(json_response));
        });
    });
};
exports['fetch_settings'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var query = 'select * from auth.internal_settings order by name';
        var sqlRequest = new sql.Request(connection);
        sqlRequest.query(query, function (err, recordset, returnValue) {
            if (err != undefined) {
                return;
            }

            var json_response = {
                status: 1,
                setting: recordset
            };
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(json_response));
        });
    });
};
exports['save_setting'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var query = 'update auth.internal_settings set value = @value where name = @name';
        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('name', sql.NVarChar(1000), request.body.name);
        sqlRequest.input('value', sql.NVarChar(4000), request.body.value);

        var local_cache = app_cache;
        var local_auth = auth_cache;
        sqlRequest.query(query, function (err, recordset, returnValue) {
            if (err != undefined) {
                return;
            }

            var json_response = {
                status: 1
            };
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(json_response));
            
            local_auth.init_cache(local_cache);
        });
    });
};
exports['post_s3_blob'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('file_path', sql.NVarChar(1000), request.body.file_path);
        sqlRequest.execute('auth.admin_upload_policy', function (err, recordset, returnValue) {
            if (err != undefined) {
                return;
            }
       
            var row = recordset[0][0];
            var private_key = row["private_key"].toString();
            var public_key = row["public_key"].toString();
            var policy_text = row["policy_text"].toString();
            var policy_base64 = Buffer(policy_text).toString('base64');
            var acl = row["acl"].toString();
            var target_file_name = row["out_file_name"].toString();
            var form_post = row["bucket_url"].toString();
            var signature = S3_generate_signature(policy_base64, private_key);
            var json_response = {policy_base64: policy_base64,
                signature: signature,
                target_file_name: target_file_name,
                AWSAccessKeyId: public_key,
                content_type: "",
                acl: acl,
                formpost: form_post,
                bucket_url: row["bucket_url"]
            };
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(json_response));

        });
    });
};
 