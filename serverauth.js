var exports = module.exports = {};
exports['roles'] = null;
exports['doc_types'] = null;
exports['permissions'] = null;
exports['internal_settings'] = {};
exports['hp_offerings'] = null;
exports['offering_lists'] = {};
exports['category_list'] = [];
exports['amenity_list'] = [];

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}
var sql = require('mssql');
exports['init_cache'] = function (app) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }
        var sqlRequest = new sql.Request(connection);
        sqlRequest.query('select * from auth.roles order by id',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log(err.message, err);
                        return;
                    }
                    exports.roles = recordsets;
                });
        sqlRequest = new sql.Request(connection);
        sqlRequest.query('select * from auth.document_types order by id',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log(err.message, err);
                        return;
                    }
                    exports.doc_types = recordsets;
                });
        sqlRequest = new sql.Request(connection);
        sqlRequest.query('select * from auth.permissions_4_doctype order by id_doctype, id_role',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log(err.message, err);
                        return;
                    }
                    exports.permissions = recordsets;
                });
        sqlRequest = new sql.Request(connection);
        sqlRequest.query('select * from auth.internal_settings order by name',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        console.log(err.message, err);
                        return;
                    }
                    for (var c=0, l=recordsets.length; c<l; c++)
                        exports.internal_settings[recordsets[c].name] = recordsets[c].value;

                    var sqlRequest = new sql.Request(connection);
                    var id_list = '';
                    var rec_list = exports.internal_settings.hp_list_recommended.split(',');
                    var trend_list = exports.internal_settings.hp_list_trending.split(',');
                    var featured_list = exports.internal_settings.hp_list_featured.split(',');
                    var blog_list = exports.internal_settings.hp_list_blogs.split(',');
                    exports.category_list = exports.internal_settings.categories_list.split(',');
                    exports.amenity_list = exports.internal_settings.amenities_list.split(',');                    
                    
                    exports.offering_lists['recommended'] = rec_list.join(',');
                    exports.offering_lists['trending'] = trend_list.join(',');
                    exports.offering_lists['featured'] = featured_list.join(',');
                    exports.offering_lists['blogs'] = blog_list.join(',');                    
                    
                    var fetch_list = [];
                    for (var c=0, l=rec_list.length; c<l; c++) {
                        var index = Number(rec_list[c]);
                        if (index > 0) {
                            if (fetch_list.contains(index) == false)
                                fetch_list.push(index);
                        }
                    }
                    for (var c=0, l=trend_list.length; c<l; c++) {
                        var index = Number(trend_list[c]);
                        if (index > 0) {
                            if (fetch_list.contains(index) == false)
                                fetch_list.push(index);
                        }
                    }
                    for (var c=0, l=featured_list.length; c<l; c++) {
                        var index = Number(featured_list[c]);
                        if (index > 0) {
                            if (fetch_list.contains(index) == false)
                                fetch_list.push(index);
                        }
                    }
                    for (var c=0, l=blog_list.length; c<l; c++) {
                        var index = Number(blog_list[c]);
                        if (index > 0) {
                            if (fetch_list.contains(index) == false)
                                fetch_list.push(index);
                        }
                    }
                    
                    
                    var id_list = fetch_list.join(',');
                    sqlRequest.query('select * from auth.documents where id in (' + id_list + ')',
                            function (err, recordsets, returnValue) {
                                if (err != undefined) {
                                    console.log(err.message, err);
                                    return;
                                }
                                exports.hp_offerings = recordsets;
                            });
                });
    });

    app.logpost('/auth/browser_token', function (request, response, next) {
        exports.browser_token(request, response, next);
    });
    app.logpost('/auth/user_create', function (request, response, next) {
        exports.user_create(request, response, next);
    });
    app.logpost('/auth/user_update', function (request, response, next) {
        exports.user_update(request, response, next);
    });
};

exports['browser_token'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('email', sql.NVarChar(1000), request.body.email);
        sqlRequest.input('pwd_hash', sql.NVarChar(1000), request.body.password);
        sqlRequest.input('access_type', sql.INT, parseInt(request.body.access_type));
        sqlRequest.execute('auth.create', function (err, recordsets, returnValue) {
            if (err != undefined) {
                return;
            }

            var resources = recordsets[1];
            var group_list = recordsets[2];
            var group_user_list = recordsets[3];

            var region = recordsets[0][0]["region"];
            region = (region == "English") ? "" : region;
            var json_response = {
                browser_token: recordsets[0][0]["token"],
                resources: resources,
                roles: exports.roles,
                doctypes: exports.doc_types,
                permissions: exports.permissions,
                book_list: group_list,
                book_member_list: group_user_list,
                hp_offerings: exports.hp_offerings,
                offering_lists: exports.offering_lists,
                amenity_list: exports.amenity_list,
                category_list: exports.category_list,
                profile_doc: {id_user: recordsets[0][0]["id_user"],
                    email: recordsets[0][0]["email"],
                    region: region,
                    bucket_name: recordsets[0][0]["bucket_name"],
                    display_name: recordsets[0][0]["display_name"],
                    allow_add: recordsets[0][0]["allow_add"],
                    user_profile: recordsets[0][0]["user_profile"],
                    private_basepath: recordsets[0][0]["private_basepath"],
                    public_basepath: recordsets[0][0]["public_basepath"],
                    bucket_url: recordsets[0][0]["bucket_url"],
                    vendor_flag: recordsets[0][0]["vendor_flag"]
                }
            };
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(json_response));
        });
    });
};
exports['user_create'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('display_name', sql.NVarChar(1000), request.body.display_name);
        sqlRequest.input('private_basepath', sql.NVarChar(1000), request.body.private_basepath);
        sqlRequest.input('public_basepath', sql.NVarChar(1000), request.body.public_basepath);
        sqlRequest.input('password_hash', sql.NVarChar(1000), request.body.password);
        sqlRequest.input('email_address', sql.NVarChar(1000), request.body.email);
        sqlRequest.input('user_profile', sql.NVarChar(1000), request.body.user_profile);
        sqlRequest.input('allow_add', sql.INT, parseInt(request.body.allow_add));
        sqlRequest.execute('auth.user_create',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    var json_response = {new_user_result: recordsets[0][0]};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
exports['user_update'] = function (request, response, next) {
    var connection = new sql.Connection(dbconfig, function (err)
    {
        if (err != undefined) {
            return;
        }

        var sqlRequest = new sql.Request(connection);
        sqlRequest.input('browser_token', sql.NVarChar(1000), request.body.browser_token);
        sqlRequest.input('display_name', sql.NVarChar(1000), request.body['display_name']);
        sqlRequest.input('region', sql.NVarChar(1000), request.body['region']);
        var allow_add = null;
        if (request.body['allow_add'] != undefined)
            allow_add = parseInt(request.body['allow_add']);
        sqlRequest.input('allow_add', sql.INT, allow_add);
        sqlRequest.input('user_profile', sql.NVarChar(sql.MAX), request.body['user_profile']);
        sqlRequest.input('password_hash', sql.NVarChar(1000), request.body['password_hash']);
        sqlRequest.input('old_password_hash', sql.NVarChar(1000), request.body['old_password_hash']);

        sqlRequest.execute('auth.user_update',
                function (err, recordsets, returnValue) {
                    if (err != undefined) {
                        return;
                    }

                    var json_response = {result: recordsets[0][0]};
                    response.writeHead(200, {"Content-Type": "application/json"});
                    response.end(JSON.stringify(json_response));
                });
    });
};
